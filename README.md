# README #

* KOSE Prediaブランドサイト内の1コンテンツ、「プレディア島」の開発リポジトリです。

### Dependencies ###

* Node.js v6.4.0+
* npm v3.10.3+

### Usage ###

* `$yarn`

##### debug build & Server Preview  # ####

* `$npm run dev`
* The exported file is stored in the `dist` folder.
*  open browser and watch [http://localhost:3000/prediaisland](http://localhost:3000/prediaisland)

##### release build  # ####

* `$npm run build`
* The exported file is stored in the `dist` folder.