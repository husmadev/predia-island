var path = require('path'),
    fs = require('fs'),
    gulp = require('gulp'),
    taskListing = require('gulp-task-listing'),
    rewrite = require('connect-modrewrite'),
    pngquant = require('imagemin-pngquant'),
    jpegoptim = require('imagemin-jpegoptim'),
    minimist = require('minimist'),
    args = minimist(process.argv.slice(2)),
    release = args.r || args.release || false,
    dir = args.d ? args.d : '',
    dist = './dist' + dir, // 出力先ディレクト
    src = './src' + dir, // ソースディレクト,
    relativeSrcPath = path.relative('.', src), // 後々、つかいま,
    serveDir = args.s ? './dist' + args.s : dist; // server動かすdirが違う場合。

module.exports = {
    src: src,
    dist: dist,
    // ---------------------
    // image
    image: {
        src: [src + '/assets/img/**/*.{jpg,png,svg,gif}'],
        dist: dist + '/assets/img',
        minify: release,
        options: {
            progressive: true,
            interlaced: true,
            svgoPlugins: [
                // { removeViewBox: false },
                // { cleanupIDs: false },
                { mergePaths: false }
            ],
            use: [
                pngquant({
                    quality: '70-80',
                    speed: 1
                }),
                jpegoptim({
                    progressive: true,
                    max: 100
                })
            ]
        }
    },
    texturepacker: {
        src: src + '/assets/texturepacker/*',
        distimg: src + '/assets/img', // srcに書き出す
        distjson: src + '/assets/js', // srcに書き出す
        options: {
            ext: '.png', // スプライトにする画像の拡張子
            imgPre: '', // 生成するスプライトのprefix
            imgExt: '.png', // 生成するスプライトの拡張子
            imgPath: '../img', // scssに記述される画像へのpath。prefixとして
            jsonExt: '.json' // 生成するcssファイルの拡張子
        }
    },
    spriteimage: {
        src: src + '/assets/spriteimage/*',
        distimg: src + '/assets/img', // srcに書き出す
        distcss: src + '/assets/scss/imgsprite', // srcに書き出す
        options: {
            ext: '.png', // スプライトにする画像の拡張子
            imgPre: 'sprite_', // 生成するスプライトのprefix
            imgExt: '.png', // 生成するスプライトの拡張子
            imgPath: '../img', // scssに記述される画像へのpath。prefixとして
            cssExt: '.scss', // 生成するcssファイルの拡張子
            cssTempl: './gulp/templates/sprite.ejs',
            // cssTempl: './gulp/templates/sprite-responsive.ejs',
            functions: false
        }
    },
    // ---------------------
    // sprite svg
    spritesvg: {
        src: src + '/assets/spritesvg/*',
        dist: src + '/assets/img',
        options: {
            symbolPrefix: '.svg-%s',
            symbolDimensions: ''
        }
    },
    // ---------------------
    // scss
    scss: {
        src: src + '/assets/scss/**/*.scss',
        dist: dist + '/assets/css',
        options: {
            sourceMap: !release,
            precision: 5,
            outputStyle: release ? 'compressed' : 'expanded',
        },
        autoprefixer: {
            browsers: ['last 5 versions', 'ie 9']
        },
        sourcemaps: {
            includeContent: !release
        }
    },
    // ---------------------
    // ejs
    ejs: {
        src: [
            src + '/**/!(_)*.ejs'
        ],
        dist: dist,
        debug: !release,
        minify: false,
        options: {
            root: src
        }
    },
    // ---------------------
    // ejs
    html: {
        src: [
            src + '/**/!(_)*.html',
            src + '/assets/js/*.html',
        ],
        dist: dist
    },
    // ---------------------
    // js
    js: {
        src: [src + '/assets/js/**'],
        dist: dist + '/assets/js',
        name: 'vender.js',
        concat: false,
        uglify: release,
        sourcemap: !release,
        sourcemaps: {
            includeContent: false
        }
    },
    // ---------------------
    // typescript
    ts: {
        confidSrc: [
            src + '/assets/ts/**/!(_)*.ts'
        ],
        src: [
            src + '/assets/ts/Headerfooter.ts'
        ],
        distname: 'foot.js',
        dist: src + '/assets/js'
    },
    // ---------------------
    // inject
    inject: {
        src: [
            src + '/**/!(_)*.ejs',
            src + '/**/*.scss'
        ],
        dist: src,
        jsdist: src + '/assets/js',
        options: {
            relative: true
        }
    },
    // ---------------------
    // copy
    copy: {
        src: [
            '!' + src + '/**/*.ejs',
            '!' + src + '/**/*.ts',
            '!' + src + '/assets/**/*.js',
            '!' + src + '/assets/js/*.html',
            '!' + src + '/assets/img/**/*',
            '!' + src + '/assets/riot/**/*', // webpackに取り込まれるためignore
            '!' + src + '/assets/spriteimage/**/*',
            '!' + src + '/assets/spritesvg/**/*',
            '!' + src + '/assets/texturepacker/**/*',
            src + '/**/*.!(scss)',
        ],
        dist: dist
    },
    // ---------------------
    // copy
    copyOtherResource: {
        src: [
            './src/root/**/*',
            './src/**/.htaccess'
        ],
        dist: 'dist'
    },
    // ---------------------
    // watch
    watch: {
        bower: './bower.json',
        js: relativeSrcPath + '/assets/js/**/*.js',
        webpack: [
            relativeSrcPath + '/assets/ts/**/*.ts'
        ],
        ts: relativeSrcPath + '/assets/ts/HeaderFooter.ts',
        riot: relativeSrcPath + '/assets/riot/**/*.tag',
        scss: relativeSrcPath + '/assets/scss/**/*.scss',
        img: [relativeSrcPath + '/assets/img/**/*'],
        ejs: [relativeSrcPath + '/**/*.ejs', './gulp/ejsconfig.json'],
        html: [relativeSrcPath + '/**/*.html'],
        other: [
            '!./src/**/*.map',
            './src/**/.htaccess',
            './src/**/*.mp4',
            './src/**/*.swf',
            './src/**/*.php',
            './src/**/*.json',
            './src/root/**/*.js'
        ]
    },
    // ---------------------
    // server
    server: {
        server: {
            baseDir: serveDir,
            // directory: false,
            index: 'index.html',
            // middleware: [
            //     rewrite([
            //         '^[^\\.]*$ /index.html [L]' // SPA mode
            //         // '^(.*)index.html$ index.html [R=301,L]' // remove index mode
            //     ])
            // ]
        },
        // reloadDebounce: 1000,
        notify: false,
        open: false, // true, 'local', 'external', 'ui', 'tunnel'
        ghostMode: {
            clicks: false,
            location: false,
            forms: false,
            scroll: false
        }
    }
}



// HELP TOOLS.
var black = '\u001b[30m';
var red = '\u001b[31m';
var green = '\u001b[32m';
var yellow = '\u001b[33m';
var blue = '\u001b[34m';
var magenta = '\u001b[35m';
var cyan = '\u001b[36m';
var white = '\u001b[37m';

if (args._[0] === 'ls' || args._[0] === 'list') {
    gulp.task('ls', taskListing);
    console.log(cyan + 'Now proccesing...');
}

if (args._[0] === undefined) {
    console.log('\n\n\n');
    console.log(cyan + '----------------------------------------');
    console.log(cyan + '-   Starting watch & server process!   -');
    console.log(cyan + '-   Have a nice development  ٩( ᐛ )و   -');
    console.log(cyan + '----------------------------------------');
    console.log('\n');
}

if (args.h || args.help) {
    console.log('\n');
    console.log(cyan + 'gulp -d=<directory path>    It specifies the root directory of the project.');
    console.log(cyan + 'gulp -s=<directory path>    Use this when the project route and the server route is different.');
    console.log(cyan + 'gulp -r || --release        Use to make the build release.');
    console.log(cyan + 'gulp -h || --help           This');
    console.log(cyan + 'gulp ls || list             Show task lists.');
    console.log('\n');
    console.log(white + 'Have a nice day ٩( ᐛ )و');
    process.exit();
}

if (!args.d) {
    console.log(white + '(・ε・)ﾃｯﾃﾚｰ ');
    console.log(red + 'Be use this argument. -> -d=<directory path>');
    console.log(red + 'If you want see the list of task -> gulp ls');
    process.exit();
}

if (!fs.existsSync(src) || !fs.statSync(src).isDirectory()) {
    console.log(white + '╭( ･ㅂ･)و ' + red + 'Project directory does not exist. : ' + src);
    process.exit();
}

if (!release && args._[0] === 'build' && !args.force) {
    console.log(white + '(　˙-˙　)? ' + red + 'Are you really order for debugging build?');
    console.log('\n');
    console.log(red + 'If add \'-release\', build for production.');
    console.log(red + 'Or add \'--force\', build for debugging.');
    process.exit();
}