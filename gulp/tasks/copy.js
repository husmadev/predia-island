var gulp = require('gulp');
var browser = require('browser-sync');
var jsonminify = require('gulp-jsonminify');
var gulpif = require('gulp-if');
var changed = require('gulp-changed');
var config = require('../config').copy;
var configCustom = require('../config').copyOtherResource;
var configCustom2 = require('../config').copyJsinsideHtml;

gulp.task('copy', function() {
    return gulp.src(config.src)
        .pipe(changed(config.dist))
        .pipe(gulpif(/\.json$/, jsonminify()))
        .pipe(gulp.dest(config.dist))
        .pipe(browser.stream());
});

gulp.task('copy:other', function() {
    return gulp.src(configCustom.src)
        .pipe(changed(configCustom.dist))
        .pipe(gulp.dest(configCustom.dist))
        .pipe(browser.stream());
});
