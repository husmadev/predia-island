var gulp = require("gulp");
var browser = require('browser-sync');
var ejs = require("gulp-ejs");
var htmlmin = require('gulp-htmlmin');
var gulpif = require('gulp-if');
var plumber = require("gulp-plumber");
var changed = require('gulp-changed');
var replace = require('gulp-replace');
var config = require('../config').ejs;

gulp.task("ejs", function() {
    var settingsFilename = '../ejsconfig.json';
    var settings = require(settingsFilename);
    let _settings = {
        "ext": ".html"
    };
    settings['debugFlg'] = config.debug;

    return gulp.src(config.src)
        .pipe(plumber())
        .pipe(changed(config.dist))
        .pipe(ejs(settings, config.options, _settings))
        .pipe(gulpif(
            config.minify,
            htmlmin({ collapseWhitespace: true })
        ))
        .pipe(gulp.dest(config.dist))
        .pipe(browser.stream());
});
