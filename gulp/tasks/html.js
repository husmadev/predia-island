var gulp = require("gulp");
var browser = require('browser-sync');
var plumber = require("gulp-plumber");
var rename = require("gulp-rename");
var changed = require('gulp-changed');
var htmlv = require('gulp-html-validator');
var config = require('../config').html;

gulp.task("html", function() {
    return gulp.src(config.src)
        .pipe(plumber())
        .pipe(changed(config.dist))
        .pipe(gulp.dest(config.dist))
        .pipe(htmlv())
        .pipe(rename({
            extname: ".json"
          }))
        .pipe(gulp.dest('./validate'))
        .pipe(browser.stream({ once: true }));
});


gulp.task("html:validate", function() {
    return gulp.src(config.src)
        .pipe(htmlv())
        .pipe(rename({
            extname: ".json"
          }))
        .pipe(gulp.dest('./validate'));
});
