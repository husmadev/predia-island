var gulp = require('gulp');
var browser = require('browser-sync');
var gulpif = require('gulp-if');
var plumber = require('gulp-plumber');
var imagemin = require('gulp-imagemin');
var changed = require('gulp-changed');
var config = require('../config').image;

gulp.task('image', function() {
    return gulp.src(config.src)
        .pipe(changed(config.dist))
        .pipe(plumber())
        .pipe(
            gulpif(
                config.minify,
                imagemin(config.options),
                imagemin(imagemin.svgo(), config.options)
            )
        )
        .pipe(gulp.dest(config.dist))
        .pipe(browser.stream());
});