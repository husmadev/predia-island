var gulp = require('gulp');
var concat = require('gulp-concat');
var inject = require('gulp-inject');
var filter = require('gulp-filter');
var mainBowerFiles = require('main-bower-files');
var config = require('../config').inject;

gulp.task('inject', function() {
    var bowerFiles = mainBowerFiles();
    var sources = gulp.src(bowerFiles, {
        read: false
    });
    console.log(bowerFiles);

    gulp.src(bowerFiles)
    .pipe(filter(['**/*.js']))
    .pipe(concat('vendor.js'))
    .pipe(gulp.dest(config.jsdist));

    return gulp.src(config.src)
        .pipe(inject(sources, config.options))
        .pipe(gulp.dest(config.dist));
});
