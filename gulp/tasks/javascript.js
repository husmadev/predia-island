var gulp = require('gulp');
var browser = require('browser-sync');
var gulpif = require('gulp-if');
var plumber = require('gulp-plumber');
var uglify = require("gulp-uglify");
var ignore = require("gulp-ignore");
var concat = require("gulp-concat");
var changed = require('gulp-changed');
var babel = require('gulp-babel');
// var mainBowerFiles = require('main-bower-files');
var sourcemaps = require('gulp-sourcemaps');
var config = require('../config').js;

gulp.task('javascript', function() {
    // var bowerFiles = mainBowerFiles('**/*.js', {
    //     debugging: true
    // });

    // return gulp.src(config.src.concat(bowerFiles))
    return gulp.src(config.src)
        .pipe(plumber())
        .pipe(changed(config.dist))
        .pipe(ignore.exclude(function(file){
            // 拡張子が.jsじゃなかったらtrueを返して除外
            return file.path.search(/.js$/) == -1;
        }))
        .pipe(sourcemaps.init())
        // .pipe(babel({
        //     plugins: ['es2015']
        // }))
        .pipe(gulpif(
            config.concat,
            concat(config.name)
        ))
        .pipe(gulpif(
            config.uglify,
            uglify({
                preserveComments: 'some'
            })
        ))
        .pipe(
            gulpif(
                config.sourcemap,
                sourcemaps.write('./map', config.sourcemaps)
            )
        )
        .pipe(gulp.dest(config.dist))
        .pipe(browser.stream());
});
