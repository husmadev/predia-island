var gulp = require('gulp');
var browserSync = require('browser-sync');
var config = require('../config').server;

gulp.task('reload', function() {
    return browserSync.reload();
});
