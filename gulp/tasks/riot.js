var gulp = require('gulp');
var browser = require('browser-sync');
var riot = require('gulp-riot');
var config = require('../config').riot;

gulp.task('riot', function() {
    return gulp.src(config.src)
        .pipe(riot())
        .pipe(gulp.dest(config.dist))
        .pipe(browser.stream());
});
