var gulp = require('gulp');
var browser = require('browser-sync');
var gulpif = require('gulp-if');
var autoprefixer = require('gulp-autoprefixer');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var config = require('../config').scss;


gulp.task('scss', function() {
    return gulp.src(config.src)
        .pipe(
            gulpif(
                config.options.sourceMap,
                sourcemaps.init()
            )
        )
        .pipe(sass(config.options).on('error', sass.logError))
        .pipe(autoprefixer(config.autoprefixer))
        .pipe(
            gulpif(
                config.options.sourceMap,
                sourcemaps.write('./map', config.sourcemaps)
            )
        )
        .pipe(gulp.dest(config.dist))
        .pipe(browser.stream({match: "**/*.css"}));
});
