var gulp = require('gulp');
var browser = require('browser-sync');
var config = require('../config').server;

gulp.task('server', function() {
    return browser(config);
});
