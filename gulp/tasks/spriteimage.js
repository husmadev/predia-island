var gulp = require('gulp');
var plumber = require('gulp-plumber');
var notify = require('gulp-notify');
var foreach = require('gulp-tap');
var runSequence = require('run-sequence');
var spritesmith = require('gulp.spritesmith');
var config = require('../config').spriteimage;

var path = require('path'),
    fs = require('fs'),
    ejs = require('ejs'),
    ms = require('merge-stream');

gulp.task('spriteimage', function (cb) {
    runSequence('imageGenerater', ['scss'], cb);
});

gulp.task('imageGenerater', function (cb) {

    var _cOptions = config.options;
    var _templ = _cOptions.cssTempl;

    // ejsのテンプレートをstringとしてcssTemplateに格納。
    if (typeof _templ === 'string' && path.extname(_templ) === '.ejs') {
        var file = fs.readFileSync(process.cwd() + '/' + _templ);
        _cOptions.cssTempl = function (data) {
            return ejs.render(file.toString(), data);
        };
    }

    return gulp.src(config.src)
        .pipe(plumber({
            errorHandler: notify.onError('<%= error.message %>')
        }))
        .pipe(foreach(function (stream, file) {
            if (file.isDirectory()) {
                var __paths = file.path.split(path.sep);
                var __name = __paths.pop();

                if (!__name) return stream;

                // retinaの場合
                var __isRetina = __name.search(/-2x$/) !== -1;
                var __isSequence = __name.search(/-sequence/) !== -1;

                // spriteのoptions設定
                var __options = {
                    cssSpritesheetName: __name,
                    imgName: _cOptions.imgPre + __name + _cOptions.imgExt,
                    cssName: '_' + __name + _cOptions.cssExt,
                    imgPath: _cOptions.imgPath + '/' + _cOptions.imgPre + __name + _cOptions.imgExt,
                    cssTemplate: _cOptions.cssTempl,
                    algorithm: __isSequence ? 'top-down' : 'binary-tree',
                    algorithmOpts: __isSequence ? {
                        sort: false
                    } : {},
                    padding: 6,
                    cssOpts: {
                        scale: __isRetina ? .5 : 1,
                        prefix: __name,
                        functions: _cOptions.functions
                    }
                };

                // 実際のstream
                var __strm = gulp.src(file.path + '/*' + _cOptions.ext)
                    .pipe(plumber())
                    .pipe(spritesmith(__options));

                __strm.img.pipe(gulp.dest(config.distimg));
                __strm.css.pipe(gulp.dest(config.distcss));

                return ms(stream, __strm);
            }

            return stream;

        }));
});
