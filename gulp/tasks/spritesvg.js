var gulp = require('gulp');
var plumber = require('gulp-plumber');
var notify = require('gulp-notify');
var foreach = require('gulp-tap');
var svgsprite = require('gulp-svg-sprite');
var config = require('../config').spritesvg;

var path = require('path'),
    fs = require('fs'),
    ejs = require('ejs'),
    ms = require('merge-stream');

gulp.task('spritesvg', function() {
    return gulp.src(config.src)
        .pipe(plumber({
            errorHandler: notify.onError('<%= error.message %>')
        }))
        .pipe(foreach(function(stream, file) {
            if (file.isDirectory()) {
                var __paths = file.path.split(path.sep);
                var __name = __paths.pop();

                if (!__name) return stream;

                var __options = {
                    // shape: {
                    //     dimension: {
                    //         precision: 2
                    //     },
                    //     // dist: 'out/intermediate-svg'
                    // },
                    mode: {
                        // css: { // Activate the symbol mode
                        //     prefix: config.options.symbolPrefix,
                        //     dimensions: config.options.symbolDimensions,
                        //     render: {
                        //         scss: {
                        //         template: './gulp/templates/svgsprite/symbol/sprite.html',
                        //             dist: '../../scss/svgsprite/_' + __name
                        //         }
                        //     }
                        // }, // Create a «css» sprite
                        // view                : true,     // Create a «view» sprite
                        // defs                : true,     // Create a «defs» sprite
                        // stack               : true,      // Create a «stack» sprite
                        symbol: { // Activate the symbol mode
                            prefix: config.options.symbolPrefix,
                            dimensions: config.options.symbolDimensions,
                            sprite: '../sprite-' + __name + '.svg',
                            bust: false,
                            render: {
                                scss: {
                                    template: './gulp/templates/svgsprite/symbol/sprite.scss',
                                    dist: '../../scss/svgsprite/_' + __name
                                }
                            },
                            example: {
                                template: './gulp/templates/svgsprite/symbol/sprite.html',
                                dist: '../../../../document/svg-' + __name + '.html'
                            }
                        }
                    }
                }

                console.log(process.cwd());
                var __strm = gulp.src(file.path + '/*.svg')
                    .pipe(plumber())
                    .pipe(svgsprite(__options))
                    .pipe(gulp.dest(config.dist));
                return ms(stream, __strm);
            }

            return stream;

        }));
});
