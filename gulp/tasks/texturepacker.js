var gulp = require('gulp');
var plumber = require('gulp-plumber');
var notify = require('gulp-notify');
var foreach = require('gulp-tap');
var spritesmith = require('gulp.spritesmith');
var sprite_texturepacker = require('spritesmith-texturepacker');
var config = require('../config').texturepacker;

var path = require('path'),
    fs = require('fs'),
    ejs = require('ejs'),
    ms = require('merge-stream');

gulp.task('texturepacker', function(cb) {
    var _cOptions = config.options;


    return gulp.src(config.src)
        .pipe(plumber({
            errorHandler: notify.onError('<%= error.message %>')
        }))
        .pipe(foreach(function(stream, file) {
            if (file.isDirectory()) {
                var __paths = file.path.split(path.sep);
                var __name = __paths.pop();

                if (!__name) return stream;

                // retinaの場合
                var __isRetina = __name.search(/-2x$/) !== -1;

                // spriteのoptions設定
                var __options = {
                    cssSpritesheetName: __name,
                    imgName: _cOptions.imgPre + __name + _cOptions.imgExt,
                    cssName: __name + _cOptions.jsonExt,
                    imgPath: _cOptions.imgPath + '/' + _cOptions.imgPre + __name + _cOptions.imgExt,
                    cssTemplate: sprite_texturepacker,
                    algorithm: 'binary-tree',
                    padding: 10,
                    cssOpts: {
                        scale: __isRetina ? .5 : 1,
                        prefix: __name
                    }
                };

                // 実際のstream
                var __strm = gulp.src(file.path + '/*' + _cOptions.ext)
                    .pipe(plumber())
                    .pipe(spritesmith(__options));

                __strm.img.pipe(gulp.dest(config.distimg));
                __strm.css.pipe(gulp.dest(config.distjson));

                return ms(stream, __strm);
            }

            return stream;

        }));
});
