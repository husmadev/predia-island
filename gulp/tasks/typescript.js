var gulp = require('gulp');
var concat = require('gulp-concat');
var browser = require('browser-sync');
var typescript = require('gulp-typescript');
var plumber = require("gulp-plumber");
var config = require('../config').ts;

var settingsFilename = '../../tsconfig.json';
var settings = require(settingsFilename);

gulp.task('typescript', function() {
    var tsconfig = typescript.createProject('tsconfig.json');
    return gulp.src(config.src)
        .pipe(typescript(tsconfig))
        .js
        .pipe(concat(config.distname))
        .pipe(gulp.dest(config.dist))
        .pipe(browser.stream());
});