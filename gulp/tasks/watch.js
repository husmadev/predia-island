var gulp = require('gulp');
var watch = require('gulp-watch');
var config = require('../config').watch;

gulp.task('watch', function() {
    // js
    // watch(config.bower, function() {
    //     gulp.start(['javascript', 'inject']);
    // });

    // js
    watch(config.js, function() {
        gulp.start(['javascript']);
    });

    // webpack
    watch(config.webpack, function() {
        gulp.start(['webpack']);
    });

    // ts
    // watch(config.ts, function() {
    //     gulp.start(['typescript']);
    // });

    // riot
    watch(config.riot, function() {
        gulp.start(['webpack']);
    });

    // scss
    watch(config.scss, function() {
        gulp.start(['scss']);
    });

    // scss
    watch(config.img, function() {
        gulp.start(['image']);
    });

    // ejs
    watch(config.ejs, function() {
        gulp.start(['ejs']);
    });

    // html
    watch(config.html, function() {
        gulp.start(['html']);
    });

    // other
    watch(config.other, function() {
        gulp.start(['copy','copy:other']);
    });
});
