var gulp = require('gulp');
var browser = require('browser-sync');
// var tsConfigUpdate = require('gulp-tsconfig-update');
var gulpif = require('gulp-if');
var uglify = require('gulp-uglify');
var plumber = require("gulp-plumber");
var notify = require('gulp-notify');
var webpackStream = require('webpack-stream');
var webpack = require("webpack");
var webpackConfig = require('../../webpack.config.js');


gulp.task('webpack', function() {
    return gulp.src('')
        .pipe(plumber({
            errorHandler: notify.onError('<%= error.message %>')
        }))
        .pipe(webpackStream( webpackConfig, webpack ))
        .pipe(gulp.dest(webpackConfig.output.path))
        .pipe(browser.stream({once: true}));
});
