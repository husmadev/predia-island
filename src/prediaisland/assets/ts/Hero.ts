import { EVENT_RESIZE, EVENT_SCROLL } from './utility/EventType';
import { PixiTextureLoader } from './plugins/pixi/PixiTextureLoader'
import * as eventemitter3 from 'eventemitter3';
import 'gsap';
import { PIXIBase } from './plugins/pixi/PixiBase';
import { log } from './utility/Utility';
import { scale } from './utility/Calc';
import * as $ from 'jquery';




/**
 *
 *
 * @export
 * @class Main
 * @extends {eventemitter3}
 */
export class Hero extends PIXIBase {

    private loader: PixiTextureLoader;
    private _texture_brightring: PIXI.Texture;
    private _texture_car: PIXI.Texture;
    private _texture_sun: PIXI.Texture;
    private _texture_sun_back: PIXI.Texture;
    private _texture_bg: PIXI.Texture;
    private _texture_dsplmnt: PIXI.Texture;
    private _texture_mask: PIXI.Texture;

    private _sprite_brightring: PIXI.Sprite;
    private _sprite_car: PIXI.Sprite;
    private _sprite_sun: PIXI.Sprite;
    private _sprite_sun_back: PIXI.Sprite;
    private _sprite_bg: PIXI.Sprite;
    private _sprite_dsplmnt: PIXI.Sprite;
    private _sprite_dsplmnt_front: PIXI.Sprite;
    private _sprite_sun_mask: PIXI.Sprite;
    private _sprite_logo_mask: PIXI.Sprite;

    private _filter_blur: PIXI.filters.BlurFilter;
    private _filter_dsplmnt: PIXI.filters.DisplacementFilter;
    private _filter_dsplmnt_front: PIXI.filters.DisplacementFilter;
    private _handler_resize = this.resize.bind(this);
    private _handler_scroll = this.scroll.bind(this);


    private _logoContainer: PIXI.Container;
    private _logoFrontSprite: PIXI.Sprite;
    private _logoBurstSprite: PIXI.extras.AnimatedSprite;

    private _suffix = scale > 1 ? '' : '';

    private _timeline: TimelineMax;

    private _resizeLock = false;


    private _scrolloffsetY = 0;

    /**
     * Creates an instance of Hero.
     *
     *
     * @memberOf Hero
     */
    constructor() {
        super('js-hero', 'herocanvas', (2430 / 2), (1460 / 2));
        this.loader = new PixiTextureLoader();
        this.loader.once(PixiTextureLoader.COMPLETE, this.loadComplete.bind(this));
        this.loader.load();
    }

    public setup(): void {
        super.setup();

        this.addStage();

        this.setupLogo();

        window.addEventListener(EVENT_SCROLL, this._handler_scroll);
        window.addEventListener(EVENT_RESIZE, this._handler_resize);
        this.resize();
        this._resizeLock = true;


        this.addTimeline();


        this.startUpdate();


        setTimeout(() => { this.startTimeline() }, 1000);
        // this.startTimeline();
    }


    public scroll(): void {

        this._scrolloffsetY = window.pageYOffset / 14;
        // if (this._resizeLock) return;
        let _scale = (this.width / this._baseWidth);

        this.rePosition(_scale);

    }

    public resize(): void {
        // if (this._resizeLock) return;

        super.resize();


        //     let _baseWidth = (this.width * 2);
        //     log(this.width , this._texture_bg.width);
        //     log(this.width / this._texture_bg.width);
        //     this._sprite_bg.scale.set((this.width * 2) / this._texture_bg.width);
        //     // this._sprite_bg.width = this.width;
        //     // this._sprite_bg.height = this._texture_bg.height * (this.width / this._sprite_bg.width);
        //     this.rePositionMinScreen();
        let _scale = this.width / this._baseWidth;

        if (window.matchMedia('(max-width:859px)').matches) {
            let __scale = this.width / (this._baseWidth + 300);

            this._logoContainer.scale.set(this.width / (this._logoContainer.width + 50));

            this._sprite_bg.scale.set(__scale);
            this._sprite_brightring.scale.set(__scale);
            this._sprite_sun.scale.set(__scale);
            this._sprite_sun_back.scale.set(__scale - .015);
        } else {
            this._sprite_bg.scale.set(_scale);
            this._sprite_brightring.scale.set(_scale);
            this._sprite_sun.scale.set(_scale);
            this._sprite_sun_back.scale.set(_scale - .015);
        }

        // this._sprite_bg.width = this.width;
        // this._sprite_bg.height = this.height;
        this._sprite_car.scale.set(_scale);
        this.rePosition(_scale);

        // }

    }


    private rePosition(_scale) {
        if (window.matchMedia('(max-width:859px)').matches) {
            this._logoContainer.position.set(this.width / 2 - this._logoContainer.width / 2, this.height - this._logoContainer.height - 30);
            this._sprite_car.position.set((45 * _scale), (this.height * .28813559322) + this._scrolloffsetY);
            this._sprite_bg.position.set(this.width, 0);

            this._sprite_brightring.position.set(
                this.width - this._sprite_brightring.width / 2 - (80 * _scale),
                this._sprite_brightring.height / 2 - (20 * _scale)
            );

            this._sprite_sun.position.set(
                this.width - this._sprite_sun.width - this._sprite_sun.width / 2 - (40 * _scale),
                (50 * _scale)
            );

        } else {
            this._logoContainer.position.set(this.width / 2 - this._logoContainer.width / 2 - (148 * _scale), this.height * .27329192547 - 50 + this._scrolloffsetY / 2);
            this._sprite_car.position.set((45 * _scale), (this.height * .46118012422) + this._scrolloffsetY);
            this._sprite_bg.position.set(this.width / 2, 0);

            this._sprite_brightring.position.set(
                this.width - this._sprite_brightring.width / 2 - (40 * _scale),
                this._sprite_brightring.height / 2 - (20 * _scale)
            );

            this._sprite_sun.position.set(
                this.width - this._sprite_sun.width - this._sprite_sun.width / 2 + (30 * _scale),
                (86 * _scale)
            );
        }

        this._sprite_sun_back.position.set(
            this._sprite_sun.position.x + 2,
            this._sprite_sun.position.y + 2
        );


    }



    private addStage(): void {

        // let tester = new PIXI.Sprite(PIXI.Texture.fromImage('/assets/img/top/hero_dmy.jpg'));
        // tester.width = this.width;
        // tester.height = this.height;
        // this.stage.addChild(tester);

        this._sprite_dsplmnt = new PIXI.Sprite(this._texture_dsplmnt);
        this._sprite_dsplmnt.scale.set(2);
        this._filter_dsplmnt = new PIXI.filters.DisplacementFilter(this._sprite_dsplmnt);
        this.stage.addChild(this._sprite_dsplmnt);

        // this._sprite_dsplmnt_front = new PIXI.Sprite(this._texture_dsplmnt);
        // this._sprite_dsplmnt_front.scale.set(20);
        // this._filter_dsplmnt_front = new PIXI.filters.DisplacementFilter(this._sprite_dsplmnt_front);
        // this.stage.addChild(this._sprite_dsplmnt_front);


        this._sprite_bg = new PIXI.Sprite(this._texture_bg);
        this._sprite_bg.name = 'bg';
        this._sprite_bg.anchor.set(.5, 1);
        // this._sprite_bg.width = this.width;
        // this._sprite_bg.height = this.height;
        this._sprite_bg.alpha = 0;
        // this._sprite_bg.filters = [this._filter_dsplmnt];
        this.stage.addChild(this._sprite_bg);



        /**
         *周りの光
         */

        this._sprite_brightring = new PIXI.Sprite(this._texture_brightring);
        this._sprite_brightring.name = 'brightring';
        this._sprite_brightring.anchor.set(.5);
        this._sprite_brightring.alpha = 0;
        // this._sprite_brightring.scale.set(.5);
        this._sprite_brightring.filters = [this._filter_dsplmnt];
        this.stage.addChild(this._sprite_brightring);


        /**
         *太陽とシスコムーン
         */
        this._sprite_sun = new PIXI.Sprite(this._texture_sun);
        this._sprite_sun.name = 'sun';
        this._sprite_sun.alpha = 0;

        this._sprite_sun_back = new PIXI.Sprite(this._texture_sun_back);
        this._sprite_sun_back.name = 'sun_back';
        this._sprite_brightring.anchor.set(.5);
        this._sprite_sun_back.alpha = 1;

        // this._sprite_sun_back.filters = [this._filter_dsplmnt_front];
        // this._sprite_sun.filters = [this._filter_dsplmnt_front];

        this.stage.addChild(this._sprite_sun_back);
        this.stage.addChild(this._sprite_sun);


        /**
         *太陽マスク
         */

        // this._sprite_sun_mask = new PIXI.Sprite(this._texture_mask);
        // this._sprite_sun_mask.name = 'sun_mask';
        // this._sprite_sun_mask.anchor.set(.5);
        // this.stage.addChild(this._sprite_sun_mask);



        /**
         *くるま
         */

        this._sprite_car = new PIXI.Sprite(this._texture_car);
        this._filter_blur = new PIXI.filters.BlurFilter(18);

        this._sprite_car.name = 'car';
        this._sprite_car.alpha = 0;
        this.stage.addChild(this._sprite_car);
        if (!window.matchMedia('(max-width:859px)').matches) {
            this._sprite_car.filters = [this._filter_blur];
        }

    }

    private addTimeline(): void {
        this._timeline = new TimelineMax();
        this._timeline.pause();

        let _scale = (this.width / this._baseWidth);

        let _carBlurUpdate = (tween) => {
            this._filter_blur.blur = tween.target.blur;
        };

        // くるま
        this._timeline.to({ blur: this._filter_blur.blur }, 2.2, {
            blur: 0,
            onUpdateParams: ['{self}'],
            onUpdate: _carBlurUpdate,
            ease: Power1.easeOut,
        }, 'car');

        this._timeline.to(this._sprite_car, 2.2, {
            alpha: 1,
            ease: Power1.easeOut,
        }, 'car');


        // this._timeline.set(this._sprite_sun, { x: '-=15px', y: '+=15px', });
        // this._timeline.set(this._sprite_sun_back, { x: '-=15px', y: '+=15px', });
        // this._timeline.set(this._sprite_brightring, { x: '-=15px', y: '+=15px', });


        this._timeline.to(this._sprite_sun, 8, {
            alpha: 1,
            ease: Power0.easeOut,
        }, 'rising');

        this._timeline.to(this._sprite_brightring, 8, {
            alpha: 1,
            ease: Power0.easeOut,
        }, 'rising');


        // this._timeline.to(this._sprite_sun, 8, {
        //     x: this.width - this._sprite_sun.width - this._sprite_sun.width / 2 + (30 * (this.width / this._baseWidth)),
        //     y: (86 * (this.width / this._baseWidth)),
        //     ease: Power1.easeOut,
        // }, 'rising');

        // this._timeline.to(this._sprite_sun_back, 8, {
        //     x: this._sprite_sun.position.x + 2,
        //     y: this._sprite_sun.position.y + 2,
        //     ease: Power1.easeOut,
        // }, 'rising');

        // this._timeline.to(this._sprite_brightring, 10, {
        //     x: this.width - this._sprite_brightring.width / 2 - (40 * (this.width / this._baseWidth)),
        //     y: this._sprite_brightring.height / 2 - (20 * (this.width / this._baseWidth)),
        //     ease: Power1.easeOut,
        // }, '-=8');



        // this._timeline.to(this._sprite_brightring.scale, 8, {
        //     x: (this.width / this._baseWidth),
        //     y: (this.width / this._baseWidth),
        //     ease: Power0.easeInOut,
        // }, '-=14');

        this._timeline.to(this._sprite_bg, 14, {
            alpha: 1,
            ease: Power1.easeOut,
        }, '-=4');

        this._timeline.to(this._logoContainer, 2, {
            alpha: 1,
            ease: Power1.easeOut,
        }, '-=11');

        this._timeline.call(this.finishTimeline.bind(this), null, null, '-=11');

    }

    private startTimeline(): void {
        this._timeline.play();
        this.fluctuationsAnimate();
        //
    }

    private finishTimeline(): void {
        this._logoBurstSprite.play();
        this._resizeLock = false;

        $('body').addClass('play-logo');
    }

    private fluctuationsAnimate(): void {

        this._sprite_dsplmnt.x += .001;
        this._sprite_dsplmnt.y += .001;
        // this._sprite_dsplmnt_front.x -= 1;
        // this._sprite_dsplmnt_front.y -= 1;
        this._sprite_dsplmnt.rotation += .002;
        requestAnimationFrame(this.fluctuationsAnimate.bind(this));
    }

    private setupLogo(): void {
        let _frames = [];

        for (let i = 44; i >= 0; i--) {
            let num = i < 10 ? '0' + i : i;
            _frames.push(PIXI.Texture.fromFrame('burstlight_' + num));
        }

        this._logoBurstSprite = new PIXI.extras.AnimatedSprite(_frames);
        this._logoBurstSprite.animationSpeed = 0.35;
        this._logoBurstSprite.loop = false;


        this._logoFrontSprite = new PIXI.Sprite(PIXI.utils.TextureCache[`logo_front`]);
        this._logoFrontSprite.anchor.set(.5);
        this._logoFrontSprite.x = this._logoBurstSprite.width / 2;
        this._logoFrontSprite.y = this._logoBurstSprite.height / 2;

        this._logoContainer = new PIXI.Container;
        this._logoContainer.alpha = 0;

        this._logoContainer.addChild(this._logoBurstSprite);
        this._logoContainer.addChild(this._logoFrontSprite);

        this.stage.addChild(this._logoContainer);

    }


    private loadComplete(e: PIXI.loaders.Resource): void {
        log(PIXI.utils.TextureCache);
        this._texture_brightring = PIXI.utils.TextureCache[`brightring${this._suffix}`];
        this._texture_car = PIXI.utils.TextureCache[`car${this._suffix}`];
        this._texture_sun = PIXI.utils.TextureCache[`sun${this._suffix}`];
        this._texture_sun_back = PIXI.utils.TextureCache[`sun_back${this._suffix}`];
        this._texture_bg = PIXI.utils.TextureCache[`bg${this._suffix}`];

        this._texture_mask = PIXI.utils.TextureCache[`gradient_mask`];

        this._texture_dsplmnt = PIXI.utils.TextureCache[`displacement`];
        this._texture_dsplmnt.baseTexture.wrapMode = PIXI.WRAP_MODES.REPEAT;
        // log(PIXI.utils.TextureCache['sprite_lightburst']);
        // $('#js-logo-burst').attr('style', `background-image:url("${PIXI.utils.TextureCache['sprite_lightburst'].baseTexture.imageUrl}")`);

        this.setup();
    }

}
