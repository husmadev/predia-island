import { ModalManager } from './plugins/ModalManager';
import { ShareSNS } from './plugins/ShareSNS';
import { log, replaceSvg2img, isElementInViewport } from './utility/Utility';
import * as eventemitter3 from 'eventemitter3';
import * as $ from 'jquery';
import 'gsap';
import 'ScrollToPlugin';
import { EVENT_MOUSE_OVER, EVENT_MOUSE_ENTER, EVENT_MOUSE_LEAVE, EVENT_CLICK, EVENT_SCROLL } from './utility/EventType';


/**
 *
 *
 * @export
 * @class Main
 * @extends {eventemitter3}
 */
export class Main extends eventemitter3 {
    private _body = $('body');

    /**
     * Creates an instance of Main.
     *
     *
     * @memberOf Main
     */
    constructor() {
        super();
    }

}

// run
window.addEventListener('DOMContentLoaded', () => {
    new Main();
});
