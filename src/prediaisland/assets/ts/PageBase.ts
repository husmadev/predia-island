import { Sticker } from './plugins/Sticker';
import * as eventemitter3 from 'eventemitter3';
import { Promise } from 'es6-promise';
import 'jquery-inview';
// import 'slick-carousel';

import { isSmartPhone, empty, log } from './utility/Utility';
import { EVENT_SCROLL, EVENT_CLICK } from './utility/EventType';

/**
 * Class PageBase
 */
export class PageBase extends eventemitter3 {

    protected body: HTMLElement;

    public pageName:string = '';

    /**
     * [constructor description]
     */
    constructor() {
        super();
        this.body = document.body;
        this.init();
    };

    /**
     * 
     */
    public init(): void {
        $('.pagetop').on('click', ()=>{
            $('html,body').stop().animate({scrollTop:0}, 750, 'easeInOutQuint');
        });
    }

}
