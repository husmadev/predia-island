import { Slider } from './plugins/Slider';
import { EVENT_SCROLL, EVENT_MOUSE_ENTER, EVENT_MOUSE_LEAVE, EVENT_MOUSE_OUT, EVENT_CLICK } from './utility/EventType';
import { log, isSmartPhone } from './utility/Utility';
import * as eventemitter3 from 'eventemitter3';
import { Promise } from 'es6-promise';
import { PageBase } from './PageBase';
import { ShareSNS } from './plugins/ShareSNS';

/**
 * Class PageSec
 */
export class PageSec extends PageBase {

    /**
     * [constructor description]
     */
    constructor() {
        super();
        this.pageName = 'sec';
        this.setup();
    };

    /**
     * 
     */
    public setup(): void {
        ShareSNS.getInstance().addHandler();

        setTimeout(()=>{
            this.showMain();
        }, 1000);
    }

    /**
     * 
     */
    protected showMain(): void {
        $('.loader').fadeOut(750);
        $('body').addClass('show');

        $('.js-inview').on('inview', (ev:any,visible:boolean)=>{
            if( visible ) {
                $(ev.currentTarget).addClass('inview');
            } else {
                $(ev.currentTarget).removeClass('inview');
            }
        });

        $('.js-menubtn').on('click', ()=>{
            $('body').toggleClass('menu-open');
        });
    }

}

// run
window.addEventListener('DOMContentLoaded', () => {
    new PageSec();
});
