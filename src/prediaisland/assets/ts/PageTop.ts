import { Slider } from './plugins/Slider';
import { EVENT_SCROLL, EVENT_MOUSE_ENTER, EVENT_MOUSE_LEAVE, EVENT_MOUSE_OUT, EVENT_CLICK } from './utility/EventType';
import { log, isSmartPhone, getCookieObject } from './utility/Utility';
import * as eventemitter3 from 'eventemitter3';
import { Promise } from 'es6-promise';
import { PageBase } from './PageBase';
import { ShareSNS } from './plugins/ShareSNS';

import 'jquery.transit';

/**
 * Class PageTop
 */
export class PageTop extends PageBase {

    protected cookies:any;

    /**
     * [constructor description]
     */
    constructor() {
        super();
        this.pageName = 'top';

        this.cookies = getCookieObject();

        if( this.cookies.getItem('visited') ) {
            this.showMain();
        } else {
            $(window).on('loadingComplete', ()=>{
                let __today:Date = new Date;
                this.cookies.setItem('visited', true, 60 * 60 * 24 );
                setTimeout(()=>{
                    this.showMain();
                }, 1000);
            });
            (window as any).motionInit();
        }
    };

    /**
     * 
     */
    protected showMain(): void {
        ShareSNS.getInstance().addHandler();
        
        $('.loader').fadeOut(750);
        $('body').addClass('show');

        $('.js-inview').on('inview', (ev:any,visible:boolean)=>{
            if( visible ) {
                $(ev.currentTarget).addClass('inview');
            } else {
                $(ev.currentTarget).removeClass('inview');
            }
        });

        $('.js-menubtn').on('click', ()=>{
            $('body').toggleClass('menu-open');
        });

        //@added 2017.12.13 告知バルーンエフェクト
        this.balloonEffect();
    }


    
    protected deg:number = 0;
    protected radius:number = 20;
    protected balloonX:number = -400;
    protected balloonY:number = 0;
    protected speed:number = 1;

    /**
     * 
     */
    protected balloonEffect(): void {
        let rad:number = Math.PI/180 * this.deg;
        this.deg += this.speed;

        if( window.innerWidth > 750 ){
            this.balloonY = ( this.radius * Math.sin(rad) );
            this.balloonX += this.speed;
        } else {
            this.balloonY = ( this.radius/2 * Math.sin(rad) );
            // this.balloonX = -150;
            this.balloonX += this.speed*0.4;
        }

        $('.notificate').css({translate:[-this.balloonX, this.balloonY]});
        

        if( this.balloonX > window.innerWidth + $('.notificate').width() ) {
            this.balloonX = -400;
        }

        window.requestAnimationFrame( this.balloonEffect.bind(this) );
    }

}

// run
window.addEventListener('DOMContentLoaded', () => {
    new PageTop();
});
