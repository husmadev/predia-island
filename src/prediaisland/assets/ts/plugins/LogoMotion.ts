import { PixiTextureLoader } from './pixi/PixiTextureLoader';
import * as eventemitter3 from 'eventemitter3';
import 'gsap';
import { PIXIBase } from './pixi/PixiBase';
import { log } from '../utility/Utility';
import { scale } from '../utility/Calc';
import * as $ from 'jquery';




/**
 *
 *
 * @export
 * @class Main
 * @extends {eventemitter3}
 */
export class Hero extends PIXIBase {

    private loader: PixiTextureLoader;
    private _texture_brightring: PIXI.Texture;
    private _texture_car: PIXI.Texture;
    private _texture_sun: PIXI.Texture;
    private _texture_sun_back: PIXI.Texture;
    private _texture_bg: PIXI.Texture;
    private _texture_dsplmnt: PIXI.Texture;

    private _sprite_brightring: PIXI.Sprite;
    private _sprite_car: PIXI.Sprite;
    private _sprite_sun: PIXI.Sprite;
    private _sprite_sun_back: PIXI.Sprite;
    private _sprite_bg: PIXI.Sprite;
    private _sprite_dsplmnt: PIXI.Sprite;
    private _sprite_dsplmnt_front: PIXI.Sprite;

    private _filter_blur: PIXI.filters.BlurFilter;
    private _filter_dsplmnt: PIXI.filters.DisplacementFilter;
    private _filter_dsplmnt_front: PIXI.filters.DisplacementFilter;

    private _suffix = scale > 1 ? '' : '';

    private _timeline: TimelineMax;

    /**
     * Creates an instance of Hero.
     *
     *
     * @memberOf Hero
     */
    constructor() {
        super('js-hero', 'herocanvas', 2430 / 2, 1460 / 2);
        this.loader = new PixiTextureLoader();
        this.loader.once(PixiTextureLoader.COMPLETE, this.loadComplete.bind(this));
        this.loader.load();
    }

    public setup(): void {
        super.setup();

        this.addStage();
        this.addTimeline();

        this.startUpdate();

        setTimeout(() => { this.startTimeline() }, 1000);
        // this.startTimeline();
    }

    private addStage(): void {

        // let tester = new PIXI.Sprite(PIXI.Texture.fromImage('/assets/img/top/hero_dmy.jpg'));
        // tester.width = this.width;
        // tester.height = this.height;
        // this.stage.addChild(tester);

        this._sprite_dsplmnt = new PIXI.Sprite(this._texture_dsplmnt);
        this._sprite_dsplmnt.scale.set(4);
        this._filter_dsplmnt = new PIXI.filters.DisplacementFilter(this._sprite_dsplmnt);
        this.stage.addChild(this._sprite_dsplmnt);

        this._sprite_dsplmnt_front = new PIXI.Sprite(this._texture_dsplmnt);
        this._sprite_dsplmnt_front.scale.set(20);
        this._filter_dsplmnt_front = new PIXI.filters.DisplacementFilter(this._sprite_dsplmnt_front);
        this.stage.addChild(this._sprite_dsplmnt_front);


        this._sprite_bg = new PIXI.Sprite(this._texture_bg);
        this._sprite_bg.name = 'bg';
        this._sprite_bg.width = this.width;
        this._sprite_bg.height = this.height;
        this._sprite_bg.alpha = 0;
        this._sprite_bg.filters = [this._filter_dsplmnt];
        this.stage.addChild(this._sprite_bg);



        /**
         *周りの光
         */

        this._sprite_brightring = new PIXI.Sprite(this._texture_brightring);
        this._sprite_brightring.name = 'brightring';
        this._sprite_brightring.anchor.set(.5);
        this._sprite_brightring.position.set(
            this.stage.width - this._sprite_brightring.width / 2 - 50 - 5,
            this._sprite_brightring.height / 2 - 20
        );
        this._sprite_brightring.alpha = 0;
        this._sprite_brightring.scale.set(.5);
        this._sprite_brightring.filters = [this._filter_dsplmnt];
        this.stage.addChild(this._sprite_brightring);


        /**
         *太陽とシスコムーン
         */
        this._sprite_sun = new PIXI.Sprite(this._texture_sun);
        this._sprite_sun.name = 'sun';
        this._sprite_sun.alpha = 0;
        this._sprite_sun.position.set(
            this.stage.width - this._sprite_sun.width - this._sprite_sun.width / 2 + 15,
            86
        );

        this._sprite_sun_back = new PIXI.Sprite(this._texture_sun_back);
        this._sprite_sun_back.name = 'sun_back';
        this._sprite_brightring.anchor.set(.5);
        this._sprite_sun_back.alpha = 1;
        this._sprite_sun_back.scale.set(.985);
        this._sprite_sun_back.position = this._sprite_sun.position;
        this._sprite_sun_back.position.set(
            this._sprite_sun_back.position.x + 2,
            this._sprite_sun_back.position.y + 2
        )

        this._sprite_sun_back.filters = [this._filter_dsplmnt_front];
        this._sprite_sun.filters = [this._filter_dsplmnt_front];

        this.stage.addChild(this._sprite_sun_back);
        this.stage.addChild(this._sprite_sun);



        /**
         *くるま
         */

        this._sprite_car = new PIXI.Sprite(this._texture_car);
        this._filter_blur = new PIXI.filters.BlurFilter(18);

        this._sprite_car.name = 'car';
        this._sprite_car.alpha = 0;
        this._sprite_car.position.set(45, this.stage.height - this._sprite_car.height);
        this.stage.addChild(this._sprite_car);
        this._sprite_car.filters = [this._filter_blur];

    }

    private addTimeline(): void {
        this._timeline = new TimelineMax();
        this._timeline.pause();

        let _carBlurUpdate = (tween) => {
            this._filter_blur.blur = tween.target.blur;
        };

        // くるま
        this._timeline.to({ blur: this._filter_blur.blur }, 2.2, {
            blur: 0,
            onUpdateParams: ['{self}'],
            onUpdate: _carBlurUpdate,
            ease: Power1.easeOut,
        }, 'car');

        this._timeline.to(this._sprite_car, 2.2, {
            alpha: 1,
            ease: Power1.easeOut,
        }, 'car');


        this._timeline.set(this._sprite_sun, { x: '-=15px', y: '+=15px', });
        this._timeline.set(this._sprite_sun_back, { x: '-=15px', y: '+=15px', });
        this._timeline.set(this._sprite_brightring, {x: '-=15px',y: '+=15px',});


        this._timeline.to(this._sprite_sun, 8, {
            alpha: 1,
            ease: Power0.easeOut,
        }, 'rising');

        this._timeline.to(this._sprite_sun_back, 8, {
            x: '+=15px',
            y: '-=15px',
            ease: Power1.easeOut,
        }, 'rising');

        this._timeline.to(this._sprite_sun, 8, {
            x: '+=15px',
            y: '-=15px',
            ease: Power1.easeOut,
        }, 'rising');


        this._timeline.to(this._sprite_brightring, 8, {
            x: '+=15px',
            y: '-=15px',
            ease: Power1.easeOut,
        }, 'rising');


        this._timeline.to(this._sprite_brightring, 8, {
            alpha: 1,
            ease: Power0.easeOut,
        }, '-=5');

        this._timeline.to(this._sprite_brightring.scale, 8, {
            x: 1,
            y: 1,
            ease: Power0.easeInOut,
        }, '-=14');

        this._timeline.to(this._sprite_bg, 14, {
            alpha: 1,
            ease: Power1.easeOut,
        }, '-=4');

        this._timeline.call(this.finishTimeline.bind(this), null, null, '-=13');

    }

    private startTimeline(): void {
        this._timeline.play();
        this.fluctuationsAnimate();
        //
    }

    private finishTimeline(): void {
        $('body').addClass('play-logo');
    }

    private fluctuationsAnimate(): void {

        // this._sprite_dsplmnt.x += .5;
        // this._sprite_dsplmnt.y += .5;
        this._sprite_dsplmnt_front.x -= 1;
        this._sprite_dsplmnt_front.y -= 1;
        this._sprite_dsplmnt.rotation += .002;
        requestAnimationFrame(this.fluctuationsAnimate.bind(this));
    }


    private loadComplete(e: PIXI.loaders.Resource): void {
        log(PIXI.utils.TextureCache);
        this._texture_brightring = PIXI.utils.TextureCache[`brightring${this._suffix}`];
        this._texture_car = PIXI.utils.TextureCache[`car${this._suffix}`];
        this._texture_sun = PIXI.utils.TextureCache[`sun${this._suffix}`];
        this._texture_sun_back = PIXI.utils.TextureCache[`sun_back${this._suffix}`];
        this._texture_bg = PIXI.utils.TextureCache[`bg${this._suffix}`];

        this._texture_dsplmnt = PIXI.utils.TextureCache[`displacement`];
        this._texture_dsplmnt.baseTexture.wrapMode = PIXI.WRAP_MODES.REPEAT;
        log(PIXI.utils.TextureCache['sprite_lightburst']);
        $('#js-logo-burst').attr('style', `background-image:url("${PIXI.utils.TextureCache['sprite_lightburst'].baseTexture.imageUrl}")`);

        this.setup();
    }

}
