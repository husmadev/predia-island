import * as eventemitter3 from 'eventemitter3';
import { addClass, disableScroll, isSmartPhone, log, zeroPadding, removeClass } from '../utility/Utility';
import { EVENT_SCROLL, EVENT_CLICK } from '../utility/EventType';
import * as Hammer from 'hammerjs';
import { scale } from '../utility/Calc';
import { YoutubePlayer } from './youtube/YoutubePlayer';

/**
 * Class ModalManager
 */
export class ModalManager extends eventemitter3 {
    private _ytp: YoutubePlayer;
    private _hammer: HammerManager;
    private _index: number;
    private _mode: any;

    private body = $('body');

    private _btn_closer = $('.js-modal-closer');
    private _btn_prev = $('#js-modal-prev');
    private _btn_next = $('#js-modal-next');

    private _content = $('#js-modal-content');
    private _modal = $('#js-modal');
    private _modalBg = $('.modal-bg');

    private _slideGroup: JQuery;

    private _btn_trigger: JQuery;

    private _youtubeCode = `<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/%%%%?rel=0&amp;showinfo=0&amp;autoplay=1" frameborder="0" allowfullscreen></iframe>`;
    private _imageCode = `<picture>
  <source media="(min-width: 1280px)" srcset="%%%%@2x.jpg 2x">
  <source media="(max-width: 861px)" srcset="%%%%-sp@2x.jpg 2x,%%%%-sp@3x.jpg 3x">
  <source media="(min-width: 860px)" srcset="%%%%.jpg 1x, %%%%@2x.jpg 2x">
  <img src="%%%%.jpg" alt="<%= alt %>">
</picture>`;

    /**
     * Creates an instance of ModalManager.
     * @param {string} trigger_name
     *
     * @memberOf ModalManager
     */
    constructor(trigger_name: string) {
        super();
        this._btn_trigger = $(trigger_name);

        this._btn_trigger.on(EVENT_CLICK, this.open.bind(this));
        this._btn_closer.on(EVENT_CLICK, this.close.bind(this));
        this._btn_prev.on(EVENT_CLICK, this.change.bind(this, 'prev'));
        this._btn_next.on(EVENT_CLICK, this.change.bind(this, 'next'));

        this._hammer = new Hammer(this._content[0]);
        this._hammer.get('swipe').set({ direction: Hammer.DIRECTION_HORIZONTAL });
        this._hammer.get('pan').set({ direction: Hammer.DIRECTION_HORIZONTAL });

        this._hammer.on('swipe', (ev: HammerInput) => {
            if (ev.deltaX > 0)
                this.change('prev');
            else
                this.change('next');
        });

        this._hammer.on('pan', (ev) => {
            // console.log('pan', ev);
        });
    };



    /**
     *
     *
     * @private
     * @param {JQueryEventObject} e
     * @returns {void}
     *
     * @memberOf ModalManager
     */
    private open(e: JQueryEventObject): void {
        if (this.body.hasClass('is-show-modal')) return;

        disableScroll(true);

        this.body.addClass('is-show-modal');

        this.openAnimation(e);

        e.preventDefault();
        return;
    }

    /**
     *
     *
     * @private
     * @param {JQueryEventObject} e
     * @returns {void}
     *
     * @memberOf ModalManager
     */
    private close(e: JQueryEventObject): void {
        if ($(e.currentTarget).hasClass('modal-bg') && isSmartPhone) return;
        this._content.removeClass('is-active');

        if (this._mode === 'movie') {
            this._ytp.stop();
            this._ytp.destroy();
        }

        this._modal.find('.modal-viewer').empty();

        this.closeAnimation(e);
    }

    /**
     *
     *
     * @private
     * @param {any} direction
     * @returns {void}
     *
     * @memberOf ModalManager
     */
    private change(direction): void {
        const _count = direction === 'prev' ? -1 : 1;
        const _Target = this._slideGroup[this._index + _count];

        if (!_Target) return;
        this._content.removeClass('is-active');

        this.setSlide(_Target);

        if (this._mode === 'movie') {
            this.playYoutube(_Target);
        } else {
            this.showPhoto(_Target);
        }
    }

    /**
     *
     *
     * @private
     * @returns {Promise<any>}
     *
     * @memberOf ModalManager
     */
    private async openAnimation(e): Promise<any> {

        TweenMax.to(this._modalBg[0], .5, {
            opacity: 1
        });

        TweenMax.to(this._btn_closer[0], .2, {
            opacity: 1,
            scale: 1
        });

        TweenMax.to(this._btn_next[0], .2, {
            opacity: 1,
        });

        TweenMax.to(this._btn_prev[0], .2, {
            opacity: 1,
        });

        // await (() => {
            if (this.isYoutube(e.currentTarget)) {
                this.playYoutube(e.currentTarget);
            } else {
                this.showPhoto(e.currentTarget);
            }

            this.setSlide(e.currentTarget);
        // })();


        TweenMax.to(this._modal.find('.modal-viewer')[0], .5, {
            scale: 1,
            opacity: 1
        });


    }

    /**
     *
     *
     * @private
     * @returns {Promise<any>}
     *
     * @memberOf ModalManager
     */
    private async closeAnimation(e): Promise<any> {

        TweenMax.to(this._btn_closer[0], .1, {
            opacity: 0,
            scale: .8
        });
        TweenMax.to(this._btn_next[0], .2, {
            opacity: 0,
        });

        TweenMax.to(this._btn_prev[0], .2, {
            opacity: 0,
        });


        TweenMax.to(this._modal.find('.modal-viewer')[0], .2, {
            scale: .98,
            opacity: 0
        });

        TweenMax.to(this._modalBg[0], .2, {
            opacity: 0
        });

        // await (() => {
            this.body.removeClass('is-show-modal');
            this._modal.removeClass(this._mode);
            disableScroll(false);
        // })();
    }

    /**
     *
     *
     * @private
     * @param {Element} _target
     * @returns {void}
     *
     * @memberOf ModalManager
     */
    private playYoutube(_target: Element): void {
        const _id = $(_target).attr('href').match(/\?v=([^&]+)/)[1].match(/^[a-zA-Z0-9\-\_]+$/)[0];
        if (!_id) return;



        this._ytp = new YoutubePlayer('js-youtube', _id, {
            // width:
        });

        if (!isSmartPhone) {
            $('#js-youtube').css({ visibility: 'hidden' });

            this._ytp.once(YoutubePlayer.STATE_PLAYING, () => {
                log('STATE_PLAYING');
                // seti
                $('#js-youtube').css({ visibility: 'visible' });

            });

            this._ytp.play();
        }

        // const _code = this._youtubeCode.replace(/%%%%/, _id);
        this._mode = 'movie';
        this._modal.addClass(this._mode);
        this._content.addClass('is-active');
                // this.appendCode(_code);

    }



    /**
     *
     *
     * @private
     * @param {Element} _target
     * @returns {void}
     *
     * @memberOf ModalManager
     */
    private showPhoto(_target: Element): void {
        let _url = $(_target).attr('href');
        if (!_url) return;

        _url = _url.replace('.jpg', '');

        const _code = this._imageCode.replace(/%%%%/g, _url);
        this._mode = 'photo';
        this.appendCode(_code);
    }


    /**
     *
     *
     * @private
     * @param {Element} _target
     * @returns {void}
     *
     * @memberOf ModalManager
     */
    private setSlide(_target: Element): void {
        const _groupname = $(_target).attr('data-group');
        this._slideGroup = this._btn_trigger.filter(function () {
            return $(this).attr('data-group') === _groupname;
        });

        this._index = this._slideGroup.index($(_target));

        this._btn_prev.removeClass('is-hide');
        this._btn_next.removeClass('is-hide');

        if (this._slideGroup.length < 2 || !_groupname) {
            this._btn_prev.addClass('is-hide');
            this._btn_next.addClass('is-hide');
        } else if (this._index === 0) {
            this._btn_prev.addClass('is-hide');
        } else if (this._index === this._slideGroup.length - 1) {
            this._btn_next.addClass('is-hide');
        }
    }

    /**
     *
     *
     * @private
     * @param {any} code
     *
     * @memberOf ModalManager
     */
    private appendCode(code): void {
        this._modal.addClass(this._mode);
        // setTimeout(() => {
            this._content.find(`.modal-viewer`).html(code);
            this._content.addClass('is-active');
        // }, 500);
        // return this._content.find(`.modal-viewer`).children();
    }


    /**
     *
     *
     * @private
     * @param {Element} _target
     * @returns {boolean}
     *
     * @memberOf ModalManager
     */
    private isYoutube(_target: Element): boolean {
        return $(_target).attr('href').match(/\?v=([^&]+)/) !== null;
    }

    /**
     *
     *
     * @private
     * @param {number} time
     * @returns {Promise<any>}
     *
     * @memberOf ModalManager
     */
    private async wait(time: number): Promise<any> {
        return new Promise(resolve => {
            setTimeout(resolve, time);
        })
    }


    /**
     *
     *
     * @private
     * @param {Object} target
     * @param {number} duration
     * @param {TweenConfig} vars
     * @returns {Promise<any>}
     *
     * @memberOf ModalManager
     */
    private awaitAnimation(target: Object, duration: number, vars: TweenConfig): Promise<any> {
        return new Promise(resolve => {
            const _comp = {
                onComplete: resolve,
                // force3D: true
                ease: Sine.easeInOut
            }
            TweenMax.to(target, duration, Object.assign(_comp, vars));
        });
    }



}
