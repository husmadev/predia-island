import * as EventType from '../utility/EventType';

/**
 * Class ShareSNS
 */
export class ShareSNS {

    /**
     * CONST MEMBER
     */

    private static readonly TWITTER_SHARE_API = 'http://twitter.com/share';
    private static readonly FACEBOOK_SHARE_API = 'http://www.facebook.com/share.php';
    private static readonly LINE_SHARE_API = 'http://line.me/R/msg/text/';
    // private static readonly LINE_SHARE_API = 'line://msg/text/';
    private static readonly WINDOW_PARAM = 'width=650,height=450,menubar=no,toolbar=no,scrollbars=yes';

    private static readonly shareURL = document.querySelector('meta[property="og:url"]').getAttribute('content');
    private static readonly shareTitle = document.title;
    private static readonly shareDescription = document.querySelector('meta[name="description"]').getAttribute('content');
    /**
     * STATIC MEMBER
     */

    // singleton instanceを格納します。
    private static _instance: ShareSNS = null;

    /**
     * PRIVATE MEMBER
     */



    private btnFacebook: HTMLElement[];
    private btnTwitter: HTMLElement[];
    private btnLine: HTMLElement[];
    private btnMail: HTMLElement[];

    private _handle_clickFB = this.click.bind(this, 'facebook');
    private _handle_clickTW = this.click.bind(this, 'twitter');
    private _handle_clickLI = this.click.bind(this, 'line');
    private _handle_clickML = this.click.bind(this, 'mal');

    private twitterParam = {
        url: ShareSNS.shareURL,
        text: (typeof (window['shareVal']) !== 'undefined' ? window['shareVal'].twitter : ShareSNS.shareDescription),
        title: '',
        hashtags: ''
    };

    private facebookParam = {
        u: ShareSNS.shareURL
    };

    private lineParam = {
        single: (typeof (window['shareVal']) !== 'undefined' ? window['shareVal'].twitter : ShareSNS.shareDescription) + ' ' + ShareSNS.shareURL
    };

    private mailParam = {
        subject: document.title,
        body: ShareSNS.shareURL
    };


    /**
     * Creates an instance of ShareSNS.
     *
     * @param {Function} caller
     *
     * @memberOf ShareSNS
     */
    constructor(caller: Function) {
        if (ShareSNS._instance)
            throw new Error('Already exists');
        else if (caller !== ShareSNS.getInstance)
            throw new Error('must use the getInstance.');

        this.init();
    }

    /**
     * セットアップの実行
     *
     * @private
     *
     * @memberOf ShareSNS
     */
    private init(): void {
        this.addHandler();
    }


    /**
     *
     * 各share buttonのイベントの追加
     *
     * @memberOf ShareSNS
     */
    public addHandler(): void {
        this.btnFacebook = <HTMLElement[]><any>document.getElementsByClassName('js-shareFB');
        this.btnTwitter = <HTMLElement[]><any>document.getElementsByClassName('js-shareTW');
        this.btnLine = <HTMLElement[]><any>document.getElementsByClassName('js-shareLI');
        this.btnMail = <HTMLElement[]><any>document.getElementsByClassName('js-shareML');


        for (var i = 0; i < this.btnFacebook.length; i++) {
            this.btnFacebook[i].addEventListener(EventType.EVENT_CLICK, this._handle_clickFB);
        }

        for (var i = 0; i < this.btnTwitter.length; i++) {
            this.btnTwitter[i].addEventListener(EventType.EVENT_CLICK, this._handle_clickTW);
        }

        for (var i = 0; i < this.btnLine.length; i++) {
            this.btnLine[i].addEventListener(EventType.EVENT_CLICK, this._handle_clickLI);
        }
        for (var i = 0; i < this.btnMail.length; i++) {
            this.btnMail[i].addEventListener(EventType.EVENT_CLICK, this._handle_clickML);
        }
    }

    /**
     * 各シェアボタンのクリックイベントを消去
     *
     *
     * @memberOf ShareSNS
     */
    public removeHandler(): void {

        for (var i = 0; i < this.btnFacebook.length; i++) {
            this.btnFacebook[i].removeEventListener(EventType.EVENT_CLICK, this._handle_clickFB);
        }

        for (var i = 0; i < this.btnTwitter.length; i++) {
            this.btnTwitter[i].removeEventListener(EventType.EVENT_CLICK, this._handle_clickTW);
        }

        for (var i = 0; i < this.btnLine.length; i++) {
            this.btnLine[i].removeEventListener(EventType.EVENT_CLICK, this._handle_clickLI);
        }

        for (var i = 0; i < this.btnMail.length; i++) {
            this.btnMail[i].removeEventListener(EventType.EVENT_CLICK, this._handle_clickML);
        }
    }


    /**
     * ボタンの種類に応じて実行する処理を分岐
     *
     * @private
     * @param {string} type
     * @param {MouseEvent} e
     *
     * @memberOf ShareSNS
     */
    private click(type: string, e: MouseEvent): void {

        let __handler = {};
        let __param = '';
        const __tgt = <HTMLAnchorElement>e.currentTarget;

        if (__tgt && __tgt.hasAttribute('data-param'))
            __param = '?' + __tgt.getAttribute('data-param');

        e.preventDefault();



        __handler['facebook'] = (e) => {
            let _url = ShareSNS.FACEBOOK_SHARE_API + this.urlFactory('facebook', __param);
            this.openWindow(_url, 'facebook');
        }

        __handler['twitter'] = (e) => {
            let _url = ShareSNS.TWITTER_SHARE_API + this.urlFactory('twitter', __param);
            // Util.log(this.currentVOL[ _private.thisVol - 1 ]);
            this.openWindow(_url, 'twitter');
        }

        __handler['line'] = (e) => {
            var _url = ShareSNS.LINE_SHARE_API + this.urlFactory('line', __param);
            location.href = _url;
        }

        __handler['mail'] = (e) => {
            let _url = this.urlFactory('mail', __param);
            location.href = 'mailto:' + _url;
        }

        __handler[type](MouseEvent);

    }


    /**
     * window.openを実行
     *
     * @private
     * @param {any} _url
     * @param {any} _string
     *
     * @memberOf ShareSNS
     */
    private openWindow(_url, _string): void {
        window.open(_url, 'shareWin' + _string, ShareSNS.WINDOW_PARAM);
    };


    /**
     * ボタンの種類に応じてurlを生成
     *
     * @private
     * @param {any} _type
     * @returns {string}
     *
     * @memberOf ShareSNS
     */
    private urlFactory(_type: string, tracking: string): string {

        let __param = '?';

        let __config = this[_type + 'Param'];

        Object.keys(__config).forEach((key, i, keys) => {
            let __combining = i < keys.length - 1 ? '&' : '';
            let _val;

            if (key === 'single') {
                __param = encodeURIComponent(__config[key]);
                return __param;
            }
            if (key === 'app') {
                __param = encodeURIComponent(__config[key]);
                return __param;
            }

            if ((<string>__config[key]).match(/^https?:\/\//))
                _val = (<string>__config[key]) + tracking;
            else
                _val = __config[key];

            __param += key + '=' + encodeURIComponent(_val) + __combining;
        });
        return __param;
    };


    /**
     * singleton
     *
     * @static
     * @returns {ShareSNS}
     *
     * @memberOf ShareSNS
     */
    public static getInstance(): ShareSNS {
        if (!ShareSNS._instance)
            ShareSNS._instance = new ShareSNS(ShareSNS.getInstance);

        return ShareSNS._instance;
    }
}
