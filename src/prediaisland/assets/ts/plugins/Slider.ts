import * as eventemitter3 from 'eventemitter3';
import * as imagesLoaded from 'imagesloaded';
/**
 * Class Slider
 */
export class Slider extends eventemitter3 {

    public static FIRSTLOAD_COMPLETE: 'FIRSTLOAD_COMPLETE' = 'FIRSTLOAD_COMPLETE';

    private is_noImageSlide: boolean;
    private container: HTMLElement;
    private parent: HTMLElement;
    private children: HTMLCollection;
    private waitTime: number;
    private current: number = 0;
    private enterFrame: number;
    private firstLoadComplete: boolean = false;
    private slideCount: number = 0;
    private startInitialized: boolean = false;
    private endInitialized: boolean = false;
    private isPlay: boolean = false;

    /**
     *
     * @param _slideElement
     */

    constructor(_slideElement: HTMLElement) {
        super();
        this.container = _slideElement;
        this.parent = this.container.parentElement;

        if (!this.container) {
            throw Error(`Can not be found. id:${this.container}`);
        }

        this.waitTime = Number(this.container.getAttribute('data-speed')) || 4000;
        this.children = this.container.children;

        // if (this.children.length < 2) {
        //     throw Error(`To specify more than one image.`);
        // }
        this.preSetup();
    }


    private preSetup(): void {
        let __firstLi = this.children[0];
        let __imgUrl = (this.parent.getAttribute('data-dir') || '') + __firstLi.getAttribute('data-src');
        if (__imgUrl === 'null') {
            this.is_noImageSlide = true;
            return;
        }
        this.imageLoader(__imgUrl);
    }

    /**
     * slideのelementを生成する。
     * 必ずImagePoolContainerで指定したコンテナ内に下記形式のDOMを記述しておく。
     * data-srcは動的にbackground-imageに変換され、読み込まれる。
     * ```
     * <ul class="js-slide" data-speed="[MILLISECONDS]">
     *     <li><img src="[IMAGE].jpg" alt="">
     *     <li data-src="[IMAGE]">
     *     <li data-src="[IMAGE]">
     *     <li data-src="[IMAGE]">
     *     <li data-src="[IMAGE]">
     * </ul>
     * ```
     * @returns void
     */
    public init(): void {
        this.startInitialized = true;

        if (this.is_noImageSlide) {
            this.imageLoadComplete();
            return;
        }

        for (let i = 0; i < this.children.length; i++) {
            let _li = this.children[i];
            let _url = (this.parent.getAttribute('data-dir') || '') + _li.getAttribute('data-src');

            _li.innerHTML = `<div class="num${i}"></div>`;
            // this.imageLoader(_url);
        }

        let _il = imagesLoaded(this.container, { background: 'div' });

        _il.on('progress', (instance, image) => {
            this.imageLoadComplete();
        //     var result = image.isLoaded ? 'loaded' : 'broken';
        //     console.log('image is ' + result + ' for ' + image.img.src);
        // });
        // _il.on('done', function (instance) {
        //     console.log('DONE  - all images have been successfully loaded');
        });
        // imagesLoaded('#container', { background: true }, function () {
        //     console.log('#container background image loaded');
        // });
    }

    /**
     *
     *
     * @private
     *
     * @memberOf Slider
     */
    private update(): void {
        if (!this.isPlay) return;

        for (let i = 0, l = this.children.length; i < l; i++) {
            this.children[i].className = this.children[i].className.replace('deactive', '');
        }

        let _pastNum: number = this.current < 1 ? this.children.length - 1 : this.current - 1;
        let _pastClassName: string = this.children[_pastNum].className;
        this.children[_pastNum].className = _pastClassName.replace('active', 'deactive');
        this.children[this.current].className += 'active';
        this.current++;
        this.current = this.current > this.children.length - 1 ? 0 : this.current;

        this.enterFrame = setTimeout(() => {
            this.update();
        }, this.waitTime);
    }

    /**
     *
     *
     * @private
     *
     * @memberOf Slider
     */
    public pause(): void {
        clearTimeout(this.enterFrame);
        this.isPlay = false;
    }


    public resume(): void {
        this.enterFrame = setTimeout(() => {
            this.play();
        }, this.waitTime);
    }

    /**
     *
     */
    private play(): void {
        if (!this.startInitialized) {
            this.init();
            return;
        }
        if (this.isPlay || !this.endInitialized) {
            return;
        }
        if (this.children.length < 2) {
            this.children[0].className += 'active';
            return;
        }
        this.isPlay = true;
        this.update();
    }


    /**
     *
     */
    private imageLoadComplete(): void {
        if (!this.startInitialized) {
            return;
        }

        this.emit(Slider.FIRSTLOAD_COMPLETE);

        this.endInitialized = true;
        this.play();
    }



    /**
     * 画像を読み込む。
     *
     * @private
     * @param {string} _src
     *
     * @memberOf Slider
     */
    private imageLoader(_src: string): void {

        // if (this.firstLoadComplete) {
        //     return;
        //     // this.imageLoadComplete();
        // }

        // let _img: HTMLImageElement = new Image();
        // _img.onload = () => {
        //     this.imageLoadComplete();
        // };
        // _img.onerror = () => {
        //     this.createErrorMessage(_src, 'Image can not be found.');
        // };
        // _img.src = _src;
    }

    public get currentIndex(): number{
        return this.current;
    }


    /**
     *
     */
    public destroy(): void {
        this.pause();
        clearTimeout(this.enterFrame);
        this.current = null;

        for (let i = 0, l = this.children.length; i < l; i++) {
            this.children[i].className = this.children[i].className.replace('deactive', '').replace('active', '');
        }
        // any
    }


    /**
     * エラーを吐く。
     * @param  {any} _element
     * @param  {any} _message
     * @returns void
     */
    private createErrorMessage(_element, _message): void {
        // let _msgElement: HTMLDivElement = document.createElement('div');
        // error(`${_element.id || _element.className || _element}\n${_message}`);
    }
}
