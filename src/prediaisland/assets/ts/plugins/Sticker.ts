import { log } from './../utility/Utility';
import * as eventemitter3 from 'eventemitter3';
import { elementPositionOnBody } from '../utility/Utility';
/**
 * Stickerの生成
 *
 * @class Sticker
 * @extends {EventEmitter2}
 */
export class Sticker extends eventemitter3 {

    /**
     * CONST MEMBER
     */
    public _FIXED_KEY = 'fixed';
    public _FIXED_STATUS_NORMAL = 'normal';
    public _FIXED_STATUS_FIXED = 'fixed';
    public _FIXED_STATUS_LOWEST = 'lowest';

    /**
     * STATIC MEMBER
     */


    /**
     * PRIVATE MEMBER
     */

    private _isFixed: boolean = false;
    private _boundingtop: number = 0;
    private _lowestPosition: number = 0;
    private _bodyHeight: number = 0;

    /**
     * PUBLIC MEMBER
     */


    /**
     * Creates an instance of Sticker.
     *
     * @param {HTMLElement} _target
     * @param {number} _offsetStartPosition fixするpositionのoffset値
     * @param {HTMLElement} [_lowestElement] fixを解除する要素。これのtopかrあoffset,_targetのheightを引いた座標までfixを継続する。
     */
    constructor(private _target: HTMLElement, private _offsetStartPosition: number, private _lowestElement?: HTMLElement) {
        super();

        this.reCalcuration();
    }


    /**
     * scrollごとに再計算を行う。ただし必要がない場合はreturn
     * 必要の判断はbodyの高さが変わったかどうかでお行う。
     *
     * @private
     * @returns {void}
     */
    private reCalcuration(): void {
        if (this._bodyHeight === document.body.offsetHeight) {
            return;
        }

        this._bodyHeight = document.body.offsetHeight;
        this._boundingtop = elementPositionOnBody(this._target).y;

        if (this._lowestElement) {
            this._lowestPosition = elementPositionOnBody(this._lowestElement).y - (this._offsetStartPosition + this._offsetStartPosition * 0.1203125) - this._target.clientHeight;
        }
    }


    /**
     * 必ず呼ぶこと。
     *
     * @returns {void}
     */
    public update(): void {
        this.reCalcuration();

        let _scrollY = (window.pageYOffset !== undefined) ? window.pageYOffset : (document.documentElement || document.body.parentNode || document.body)['scrollTop'];
        let __isInStartPosition: boolean = _scrollY >= this._boundingtop - (this._offsetStartPosition + this._offsetStartPosition * 0.1203125);

        // lowest座標を超えたらstatus change
        if (!!this._lowestPosition && _scrollY > this._lowestPosition) {
            this._target.setAttribute(this._FIXED_KEY, this._FIXED_STATUS_LOWEST);
            this._isFixed = false;
            return;
        }

        // start positionを超えたらfixed。fixed済の場合はskip
        if (__isInStartPosition) {
            if (this._isFixed) {
                return;
            }

            this._target.setAttribute(this._FIXED_KEY, this._FIXED_STATUS_FIXED);
            this._isFixed = true;
        } else {
            this._target.setAttribute(this._FIXED_KEY, this._FIXED_STATUS_NORMAL);
            this._isFixed = false;
        }
    }


    public destroy(): void {
        this._target = null;
        this._lowestElement = null;
    }
}
