import { EVENT_SCROLL, EVENT_CLICK, EVENT_RESIZE } from '../utility/EventType';
import { log, isSmartPhone } from '../utility/Utility';
import * as eventemitter3 from 'eventemitter3';

/**
 * Class TabContent
 */
export class TabContent extends eventemitter3 {

    public static STATE_ACTIVE = 'is-active';

    private _btn_TabToggle:JQuery = $('.js-tabToggle a');
    private _tabContent:JQuery = $('.js-tabContent');
    private _tabContainer:JQuery = $('#js-tabContainer');

    private _scrollto: JQuery;


    private _selectedId: string = null;

    /**
     * [constructor description]
     */
    constructor(defaultSelectID: string,$scrollDefault:JQuery) {
        super();

        this._scrollto = $scrollDefault;

        window.addEventListener(EVENT_RESIZE, this.resize.bind(this));
        this._btn_TabToggle.on(EVENT_CLICK, this.changeContent.bind(this));
        this.changeContent(defaultSelectID);
    };


    private changeContent(e: JQueryEventObject | string): void {

        if (typeof e !== 'string') {
            this._selectedId = $(e['currentTarget']).attr('href');
            e['preventDefault']();
        } else {
            this._selectedId = e;
        }

        this._btn_TabToggle.removeClass(TabContent.STATE_ACTIVE);
        this._tabContent.removeClass(TabContent.STATE_ACTIVE);

        this._btn_TabToggle.filter((i, ele) => {
            return $(ele).attr('href') === `${this._selectedId}`;
        }).addClass(TabContent.STATE_ACTIVE);

        if (typeof e !== 'string') {
            TweenMax.to(window, .3, {
                scrollTo: this._scrollto.offset().top - (isSmartPhone ? 66 : 104),
                autoKill: false,
                esae: Quint.easeInOut,
                delay: .2,
                onComplete: () => { this.changeContentheight() },
            });
        } else {
            setTimeout(() => {
                this.changeContentheight();
            }, 200);
        }
    }

    private changeContentheight(): void {

        const _activeContent = this._tabContent.filter((i, ele) => {
            return $(ele).attr('id') === `${this._selectedId}`.replace('#', '');
        });
        _activeContent.addClass(TabContent.STATE_ACTIVE);

        this._tabContainer.height(_activeContent.height());
    }

    public resize(): void {
        this.changeContentheight();
    }


}
