import { EVENT_RESIZE } from './utility/EventType';
import { log } from './utility/Utility';
import * as eventemitter3 from 'eventemitter3';
import { scale } from './utility/Calc';
import 'pixi.js';

/**
 * Class PIXIBase
 */
export class PIXIBase extends eventemitter3 {

    public stage: PIXI.Container;
    private _renderer: PIXI.WebGLRenderer | PIXI.CanvasRenderer; // <- that is autoDetectRenderer
    private _stageAppendDOMID: string;
    public width: number;
    public height: number;
    public _baseWidth: number;
    public _baseHeight: number;

    private _stageID: string;

    private _wrapper: HTMLElement;


    private _RAF: number;

    public frameCount: number;


    /**
     * @param  {any} _stageAppendDOMID ステージをappendするDOM。
     * @param  {any} _stageID stage canvasのID
     * @param  {any} _width stageのwidth
     * @param  {any} _height stageのheight
     */
    constructor(_stageAppendDOMID: string, _stageID: string, _width: number, _height: number) {
        super();
        this._stageAppendDOMID = _stageAppendDOMID;
        this._stageID = _stageID;
        this._baseWidth = _width;
        this._baseHeight = _height;
        this.frameCount = 0;

        this.createStage();
    }


    /**
     * @returns void
     */
    private createStage(): void {
        this._renderer = PIXI.autoDetectRenderer(this._baseWidth, this._baseHeight, {
            // backgroundColor: 0xffffff,
            antialias: true, //scale === 1 ? true : false,
            transparent: true,
            autoResize: true,
            resolution: scale
        });

        this._renderer.view.style.width = this._renderer.width / scale + 'px';
        this._renderer.view.style.height = this._renderer.height / scale + 'px';
        this._renderer.view.id = this._stageID;
        this._wrapper = document.getElementById(this._stageAppendDOMID);
        this._wrapper.appendChild(this._renderer.view);
        this.stage = new PIXI.Container();
        this.stage.interactive = false;

        this.width = this._renderer.width / scale;
        this.height = this._renderer.height / scale;
    }


    public setup(): void {

        // do something.
    }


    /**
     *
     *
     * @param {boolean} [bool]
     */
    private renderer(bool: boolean = true): void {
        this._renderer.render(this.stage);
        this._RAF = requestAnimationFrame(() => { this.renderer(); });
    }


    public resize(e?, _width: number = this._wrapper.clientWidth, _height: number = this._wrapper.clientHeight): void {

        // let ratio = Math.min(_width / this._baseWidth, _height / this._baseHeight);

        if (window.matchMedia('(max-width:859px)').matches) {
            // height base
            let ratio = _width / this._baseWidth;

            // Scale the view appropriately to fill that dimension
            // this.stage.scale.x = this.stage.scale.y = ratio;

            // Update the renderer dimensions
            this._renderer.resize(
                this._wrapper.clientWidth + 120,
                this._wrapper.clientWidth * 0.9375
            );

        } else {
            // height base
            let ratio = _height / this._baseHeight;

            // Scale the view appropriately to fill that dimension
            // this.stage.scale.x = this.stage.scale.y = ratio;

            // Update the renderer dimensions
            this._renderer.resize(
                Math.ceil(this._baseWidth * ratio),
                Math.ceil(this._baseHeight * ratio)
            );
        }



        this.width = this._renderer.width / scale;
        this.height = this._renderer.height / scale;
        this._renderer.view.style.width = this._renderer.width / scale + 'px';
        this._renderer.view.style.height = this._renderer.height / scale + 'px';
    }

    public pauseUpdate(): void {
        cancelAnimationFrame(this._RAF);
    }

    public startUpdate(): void {
        cancelAnimationFrame(this._RAF);
        this._RAF = requestAnimationFrame(() => { this.renderer(); });
    }

    public destroy(): void {
        cancelAnimationFrame(this._RAF);
        // window.removeEventListener(EVENT_RESIZE, this._handler_resize);
        this.stage.destroy(true);
        this._renderer.destroy(true);
        PIXI.utils.TextureCache = {};
        PIXI.utils.BaseTextureCache = {};
    }
}
