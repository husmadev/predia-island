import { scale } from './utility/Calc';
import * as eventemitter3 from 'eventemitter3';
import 'pixi.js';
import { log, error } from './utility/Utility';

/**
 * Class PIXIBase
 */
export class PixiTextureLoader extends eventemitter3 {

    public static COMPLETE: 'complete' = 'complete'; // pixi.loadersの予約
    public static LOAD: 'load' = 'load'; // pixi.loadersの予約
    public static ERROR: 'error' = 'error'; // pixi.loadersの予約
    public static PROGRESS: 'progress' = 'progress'; // pixi.loadersの予約

    private static BASE_URL: string = '/assets/';
    private static FILE_EXT: string = '.json';

    private _suffix = scale > 1 ? '' : '';

    private _loader: PIXI.loaders.Loader;
    // private _callback: Function;

    constructor() {
        super();

        this._loader = new PIXI.loaders.Loader(PixiTextureLoader.BASE_URL);

        // set sprite sheet
        this._loader
            .add(
                `hero${this._suffix}`,
                `js/hero${this._suffix}${PixiTextureLoader.FILE_EXT}`
            )
            .add(
                'bg',
                `img/top/burst${this._suffix}.jpg`
            )
            .add(
                'displacement',
                `img/top/displacement.png`
            )

            .add(
                'logo_front',
                `img/top/logo.png`
            )
            .add(
                'gradient_mask',
                `img/top/mask.png`
            )
            .add(
                'burstlight',
                `js/burstlight.json`
            );

        this._loader.on(PixiTextureLoader.COMPLETE, this.loadComplete.bind(this));
        this._loader.on(PixiTextureLoader.ERROR, this.loadError.bind(this));
        this._loader.on(PixiTextureLoader.PROGRESS, this.loadProgress.bind(this));
    }

    public load() {
        // this._callback = callback;
        this._loader.load();

    }

    private loadProgress(loader: PIXI.loaders.Loader, resource: PIXI.loaders.Resource): void {
        log('loadProgress', loader.progress);
    }


    private loadError(error, loader: PIXI.loaders.Loader, resource: PIXI.loaders.Resource): void {
        log('loadError', error);
    }


    /**
     * 読み込み完了後、callbackを実行し、格納していた結果を削除。
     * 次のloadイヴェントに備える。
     *
     * @private
     * @param {any} e
     */
    private loadComplete(loader: PIXI.loaders.Loader, object: PIXI.Texture[]): void {
        log('loadComplete!', loader, object);
        this.emit(PixiTextureLoader.COMPLETE, object);
        // this.*aseventemitter3
        // this._callback(loader, object);
        this.destroy();
    }

    public destroy(): void {
        // this._callback = null;
        this._loader = null;
    }

}
