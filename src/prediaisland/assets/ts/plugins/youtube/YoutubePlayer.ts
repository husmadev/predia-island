import { log, info } from './../../utility/Utility';
import { EVENT_RESIZE } from './../../utility/EventType';
import * as eventemitter3 from 'eventemitter3';
import { YoutubePlayerChief } from './YoutubePlayerChief';
import 'gsap';

/**
 * @fileoverview YoutubePlayerの生成、制御をする。
 * YoutubePlayerChiefに依存している。
 */



interface IPlayerOptions {
    width?: number;
    height?: number;
    fullscreen?: boolean;
    loop?: boolean;
    pointerEvents?: boolean;
    cover?: string;
    mute?: boolean;
    fouceEndtime?: number;
    aspect?: {
        w: number;
        h: number;
    };
}

// interface Object{
//      assign<T, U>(target: T, source: U): T & U;
//     assign<T, U, V>(target: T, source1: U, source2: V): T & U & V;
//     assign<T, U, V, W>(target: T, source1: U, source2: V, source3: W): T & U & V & W;
//     assign(target: any, ...sources: any[]): any;
// }

/**
 *
 *
 * @class YoutubePlayer
 * @extends {EventEmitter2}
 */
export class YoutubePlayer extends eventemitter3 {


    /**
     * CONST MEMBER
     */

    public static STATE_READY = 'PLAYER_STATE_READY';
    public static STATE_ENDED = 'PLAYER_STATE_ENDED';
    public static STATE_PLAYING = 'PLAYER_STATE_PLAYING';
    public static STATE_PAUSED = 'PLAYER_STATE_PAUSED';
    public static STATE_BUFFERING = 'PLAYER_STATE_BUFFERING';
    public static STATE_CUED = 'PLAYER_STATE_CUED';


    /**
     * STATIC MEMBER
     */


    /**
     * PRIVATE MEMBER
     */

    // appendするcontainer
    private _container: HTMLElement;

    // contanerにappendするiframe
    private _iframe: HTMLIFrameElement;

    private _iframeIdPrefix: string = 'player-';

    // チーフ
    private _ytChief: YoutubePlayerChief;

    // 自分のもつyoutubeplayer
    private _player: YT.Player;

    // video IDを保存
    private _videoid: string;
    private _videouid: string;

    // 他と一緒に再生するかどうか。
    private _withSome: boolean = false;

    // 解像度の選択肢
    private _sizeList = {
        hd1080: 1080,
        hd720: 720,
        large: 480,
        medium: 360,
        small: 240,
        tiny: 144
    };

    // アスペクト
    private _aspect = (16 / 9);

    private _options: IPlayerOptions = {
        aspect: {
            w: 16,
            h: 9
        },
        mute: false
    };

    private _isMute: boolean = false;

    private _isReady: boolean = false;
    private _isReadyTimeout: number;

    private _fadeSoundTween: TweenMax;
    private _volume: number = 80; // 0-100
    private _latestVolume: number = 1;

    private _raf: number;

    private _fouceEndtime: number = 0; // seconds


    private _handle_resize = this.resize.bind(this);
    private _handle_onPlayerReady = this.onPlayerReady.bind(this);
    private _handle_onPlayerStateChange = this.onPlayerStateChange.bind(this);
    private _handle_onPlayerQualityChange = this.onPlayerQualityChange.bind(this);

    private _isPlay = false;

    private _playerState: YT.PlayerState;

    private _checkPlayingStateInterval: number;

    private _firstBuffering = true;

    /**
     * PUBLIC MEMBER
     */



    /**
     * Creates an instance of YoutubePlayer.
     *
     * @param {Function} caller
     *
     * @memberOf YoutubePlayer
     */
    constructor(containerid: string, videoid: string, options?: IPlayerOptions) {
        super();
        log('YoutubePlayer:::constructor');

        if (!document.getElementById(containerid)) {
            throw Error('container is not exist.');
        }

        this._options = Object.assign(this._options, options);
        log('YoutubePlayer:::constructor2');

        this._container = document.getElementById(containerid);
        this.changeState('ready');
        this._videoid = videoid;
        this._videouid = this._iframeIdPrefix + this._videoid + (~~(new Date()));

        this._ytChief = YoutubePlayerChief.getInstance();
        this._ytChief.onReadyCallBack(this.init.bind(this));

    }

    /**
     * YT.Playerのセットアップ。
     *
     * @private
     * @returns {YoutubePlayer}
     *
     * @memberOf YoutubePlayer
     */
    private init(): YoutubePlayer {
        log('YoutubePlayer:::init');

        let __forIframeElement: HTMLDivElement = document.createElement('div');
        __forIframeElement.id = this._videouid;
        // __forIframeElement.style.backgroundImage = `url(//img.youtube.com/vi/${this._videoid}/maxresdefault.jpg)`;
        // __forIframeElement.style.backgroundSize = 'cover';

        this._container.appendChild(__forIframeElement);

        this._player = new YT.Player(__forIframeElement.id, {
            videoId: this._videoid,
            width: this._options.width,
            height: this._options.height,
            playerVars: {
                autoplay: 0,
                controls: 1,
                enablejsapi: 1,
                iv_load_policy: 3,
                disablekb: 1,
                showinfo: 0,
                rel: 0,
                start: 0,
            }
        });

        this._player.addEventListener('onReady', this._handle_onPlayerReady);
        this._player.addEventListener('onStateChange', this._handle_onPlayerStateChange);
        this._player.addEventListener('onPlaybackQualityChange', this._handle_onPlayerQualityChange);

        this._iframe = <HTMLIFrameElement>this._player['getIframe']();
        // this._iframe.style.opacity = '0';

        this._ytChief.addPlayer(this._player, this._videouid);
        return this;
    }


    // TODO: ロゴマークきえるようにリサイズする
    private resize(): void {
        let __containerWidth: number = this._container.offsetWidth;
        let __containerHeight: number = this._container.offsetHeight;

        let __width: number = 0;
        let __height: number = 0;

        if (__containerWidth / this._options.aspect.w * this._options.aspect.h > __containerHeight) {
            // 盾を拡大
            __width = __containerWidth;
            __height = __containerWidth / this._options.aspect.w * this._options.aspect.h;
        } else {
            // 横を拡大
            __width = __containerHeight / this._options.aspect.h * this._options.aspect.w;
            __height = __containerHeight;
        }

        this._iframe.style.width = `${Math.floor(__width) + 2}px`;
        this._iframe.style.height = `${Math.floor(__height) + 200}px`;
    }


    /**
     * 再生する映像の差し替え。
     *
     * @param {string} videoid
     *
     * @memberOf YoutubePlayer
     */
    public changeVideo(videoid: string): YoutubePlayer {
        if (!this._player) {
            this._videoid = videoid;
            this.init();
        } else {
            this._player.loadVideoById(videoid);
        }
        return this;
    }


    /**
     * 再生を開始する。
     *
     * @param {number} [secounds=0] 再生位置
     * @param {boolean} [withSome=false] 他と一緒に再生するか
     * @returns {YoutubePlayer}
     *
     * @memberOf YoutubePlayer
     */
    public play(secounds: number = 0, withSome: boolean = false): YoutubePlayer {
        clearTimeout(this._isReadyTimeout);

        if (!this._isReady) {
            this._isReadyTimeout = setTimeout(() => {
                this.play(secounds, withSome);
            }, 1000);
            return;
        }

        this._withSome = withSome;
        // this._iframe.style.opacity = '1';

        if (!secounds) {
            this._player.playVideo();
            this._isPlay = true;
        } else {
            this._player.seekTo(secounds, true);
        }

        if (!withSome) {
            this._ytChief.stopOther(this._videouid);
        }
        return this;
    }


    /**
     * 再生を停止する
     *
     * @returns {YoutubePlayer}
     *
     * @memberOf YoutubePlayer
     */
    public stop(): YoutubePlayer {
        this._player.stopVideo();
        this._isPlay = false;
        return this;
    }


    public pause(): void {
        this._player.pauseVideo();

        // if (this._isPlay) {
        //     this._player.pauseVideo();
        // } else {
        //     this._player.playVideo();
        // }
        this._isPlay = false;
    }

    public resume(): void {
        this._player.playVideo();

        // if (this._isPlay) {
        //     this._player.pauseVideo();
        // } else {
        //     this._player.playVideo();
        // }
        this._isPlay = true;
    }


    /**
     * ボリュームを指定しる。
     *
     * @param {number} [volume=null]
     * @returns {YoutubePlayer}
     *
     * @memberOf YoutubePlayer
     */
    public changeVolume(volume: number): YoutubePlayer {

        this._player.setVolume(volume);
        return this;
    }


    /**
     * 音量をフェードイン/アウトでmute/unmuteを切り替える。
     *
     * @param {boolean} isMute
     * @returns {YoutubePlayer}
     *
     * @memberOf YoutubePlayer
     */
    public mute(isMute: boolean, globalMute: boolean = true): YoutubePlayer {
        if (this._fadeSoundTween) {
            this._fadeSoundTween.pause();
        }

        if (isMute) {
            this._latestVolume = this._player.getVolume();
            this._fadeSoundTween = TweenMax.to(this, .5, {
                '_volume': 0,
                onUpdate: () => {
                    this._player.setVolume(this._volume);
                },
                onComplete: () => {
                    this._player.mute();
                }
            });
        } else {
            this._player.unMute();
            this._player.setVolume(0);
            this._fadeSoundTween = TweenMax.to(this, .5, {
                '_volume': 80,
                onUpdate: () => {
                    this._player.setVolume(this._volume);
                }
            });
        }
        this._isMute = isMute;
        if (globalMute) {
            this._ytChief._isMute = isMute;
        }

        return this;
    }

    public getStatusMute(): boolean {
        return this._isMute;
    }


    /**
     * 進捗をパーセントで返す
     *
     * @returns {number} 0.0~1.0
     *
     * @memberOf YoutubePlayer
     */
    public getProgress(): number {
        if (!this._isReady) {
            return 0;
        }
        return this._player.getCurrentTime() / this._player.getDuration();
    }

    public getCurrentTime(): number {
        if (!this._isReady) {
            return;
        }

        return this._player.getCurrentTime();
    }

    /**
     * パーセントを渡すとその割合へseekする。
     *
     * @param {number} percentage 0.0~1.0
     * @returns {YoutubePlayer}
     *
     * @memberOf YoutubePlayer
     */
    public getPersent2Sec(percentage: number): number {
        if (!this._isReady) {
            return;
        }
        return percentage * this._player.getDuration();
    }
    /**
     * パーセントを渡すとその割合へseekする。
     *
     * @param {number} percentage 0.0~1.0
     * @returns {YoutubePlayer}
     *
     * @memberOf YoutubePlayer
     */
    public seekToPersent(percentage: number): YoutubePlayer {
        log('seekTo', this._isReady);
        if (!this._isReady) {
            return;
        }

        let __secound = percentage * this._player.getDuration();
        log('seekTo', __secound, this._player.getDuration());

        // this._iframe.style.opacity = '1';
        this._player.seekTo(__secound, true);
        // this._player.playVideo();
        return this;
    }

    /**
     * パーセントを渡すとその割合へseekする。
     *
     * @param {number} percentage 0.0~1.0
     * @returns {YoutubePlayer}
     *
     * @memberOf YoutubePlayer
     */
    public seekToSeconds(second: number): YoutubePlayer {
        log('seekTo', this._isReady);
        if (!this._isReady) {
            return;
        }

        log('seekTo', second, this._player.getDuration());

        // this._iframe.style.opacity = '1';
        this._player.seekTo(second, true);
        return this;
    }


    /**
     * objectを破棄する。
     *
     * @returns {YoutubePlayer}
     *
     * @memberOf YoutubePlayer
     */
    public destroy() {
        this._ytChief.removePlayer(this._videouid);

        this._handle_onPlayerReady = function () { /* any */ };
        this._handle_onPlayerStateChange = function () { /* any */ };
        this._handle_onPlayerQualityChange = function () { /* any */ };

        // TODO : インスタンスを破棄するとシーン遷移がおかしくなる&どうせ大元でnullするから破棄されるしOK。
        this._player.stopVideo();
        this._player.clearVideo();
        this._player.destroy();

        cancelAnimationFrame(this._raf);
        clearInterval(this._checkPlayingStateInterval);

        window.removeEventListener(EVENT_RESIZE, this._handle_resize);

        this._player = null;
    }


    /**
     * YT.Player instanceの準備が完了したらcall
     *
     * @private
     * @param {any} e
     *
     * @memberOf YoutubePlayer
     */
    private onPlayerReady(e): void {
        this._isReady = true;

        // this._player.mute();
        // this._player.playVideo();
        // this._player.stopVideo();
        // this._player.unMute();

        this._fouceEndtime = this._options.fouceEndtime || this._player.getDuration() - .5;

        if (this._options.mute || this._ytChief._isMute) {
            this._player.mute();
            this._isMute = true;
        }

        if (this._options.fullscreen) {
            this._iframe.style.width = '100%';
            this._iframe.style.height = '100%';
            this._iframe.style.position = 'absolute';
            this._iframe.style.top = '50%';
            this._iframe.style.left = '50%';
            this._iframe.style.transform = 'translate(-50%,-50%)';
            this._iframe.style['msTransform'] = 'translate(-50%,-50%)';
            this._iframe.style['webkitTransform'] = 'translate(-50%,-50%)';

            window.addEventListener(EVENT_RESIZE, this._handle_resize);
            this.resize();
        }

        // TODO: 少し進めてバッファを終わらせる。再生ボタンも隠したい。

        this._player.setVolume(this._volume);

        this.emit(YoutubePlayer.STATE_READY, this);
    }


    /**
     * クオリティの変更があった倍位にcallback
     *
     * @private
     * @param {any} e
     *
     * @memberOf YoutubePlayer
     */
    private onPlayerQualityChange(e): void {
        log(e.data);
    }


    /**
     * YoutubePlayerのstatus変更でcall
     * それぞれのeventもemit.
     *
     * @private
     * @param {any} e
     *
     * @memberOf YoutubePlayer
     */
    private onPlayerStateChange(e): void {

        this.checkVideoQuality();

        switch (e.data) {
            case YT.PlayerState.ENDED:
                /* 0 (再生終了（＝最後まで再生した）) */
                info(`YT.PlayerState.ENDED ID:${this._videoid}`);
                if (this._options.loop) {
                    this._player.playVideo();
                    this._isPlay = true;
                }
                this.emit(YoutubePlayer.STATE_ENDED, this);
                break;
            case YT.PlayerState.PLAYING:
                /* 1 (再生中) */
                if (!this._withSome) {
                    this._ytChief.stopOther(this._videouid);
                }
                info(`YT.PlayerState.PLAYING ID:${this._videoid}`);
                clearInterval(this._checkPlayingStateInterval);
                this.changeState('play');
                this.addListenerForEND();
                this.emit(YoutubePlayer.STATE_PLAYING, this);
                break;
            case YT.PlayerState.PAUSED:
                /* 2 (一時停止された) */
                info(`YT.PlayerState.PAUSED ID:${this._videoid}`);
                this.emit(YoutubePlayer.STATE_PAUSED, this);
                break;
            case YT.PlayerState.BUFFERING:
                /* 3 (バッファリング中) */
                info(`YT.PlayerState.BUFFERING ID:${this._videoid}`);
                // if (this._firstBuffering) {
                //     this._player.seekTo(this._player.getCurrentTime() + .1, true);
                //     this._firstBuffering = false;
                // }
                this.checkPlaying();
                this.emit(YoutubePlayer.STATE_BUFFERING, this);
                break;
            case YT.PlayerState.CUED:
                /* 5 (頭出しされた) */
                info(`YT.PlayerState.CUED ID:${this._videoid}`);
                this.emit(YoutubePlayer.STATE_CUED, this);
                break;
            case -1:
                /* -1 (未スタート、他の動画に切り替えた時など) */
                // this._player.playVideo(); /* 再生開始 */
                break;
        }
    }


    /**
     * HD1080じゃない場合でもscreenのサイズがfullHDっぽい場合は強制的にfullHDにする。
     *
     * @private
     *
     * @memberOf YoutubePlayer
     */
    private checkVideoQuality(): void {
        let __loaded = this._player.getVideoLoadedFraction();
        if (__loaded > 0) {
            let __levels = this._player['getAvailableQualityLevels']();
            let __current = this._player['getPlaybackQuality']();
            log(__current);
            if (window.screen.height > 1000 && __current !== 'hd1080') {
                this._player['setPlaybackQuality']('hd1080');
            }
        }
    }


    /**
     *
     * BUFFERINGのままPLAYING stateが帰ってこないことがあるので
     * 万が一に備えて強制的にcheckする。
     * invervalはPLAYINGステータスの中で解除する。
     *
     * @private
     *
     * @memberOf YoutubePlayer
     */
    private checkPlaying(): void {
        this._checkPlayingStateInterval = setInterval(() => {
            var state = this._player.getPlayerState();
            if (YT.PlayerState.PLAYING === state) {
                this.onPlayerStateChange({
                    data: state,
                    test: true
                });
            }
        }, 1000);
    }



    private addListenerForEND(): void {
        this._raf = requestAnimationFrame(this.addListenerForEND.bind(this));

        if (this._player && this._player.getCurrentTime() >= this._fouceEndtime && !this._options.loop) {
            cancelAnimationFrame(this._raf);
            this.emit(YoutubePlayer.STATE_ENDED, this);
            this.changeState('ended');
            this._container.setAttribute('state', 'ended');
            return;
        }

    }

    private changeState(state: string): void {
        this._container.setAttribute('state', state);
    }
}


if (typeof Object.assign !== 'function') {
    (function () {
        Object.assign = function (target) {
            'use strict';
            if (target === undefined || target === null) {
                throw new TypeError('Cannot convert undefined or null to object');
            }

            var output = Object(target);
            for (var index = 1; index < arguments.length; index++) {
                var source = arguments[index];
                if (source !== undefined && source !== null) {
                    for (var nextKey in source) {
                        if (Object.prototype.hasOwnProperty.call(source, nextKey)) {
                            output[nextKey] = source[nextKey];
                        }
                    }
                }
            }
            return output;
        };
    })();
}
