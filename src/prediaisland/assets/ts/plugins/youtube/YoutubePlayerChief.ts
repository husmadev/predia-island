import { log } from './../../utility/Utility';
/**
 * @fileoverview YoutubePlayerで生成された
 * Objectの管理とAPIの生成を行う。
 */


interface IPlayerStack {
    id: string;
    player: YT.Player;
}

/**
 * Class YoutubePlayerInitialyzer
 */
export class YoutubePlayerChief {

    /**
     * CONST MEMBER
     */


    /**
     * STATIC MEMBER
     */

    // singleton instanceを格納します。
    private static _instance: YoutubePlayerChief = null;


    /**
     * PRIVATE MEMBER
     */

    // callbackを貯めとく場所
    private _callbackStack: Function[] = [];

    // callbackを再帰的に処理していくためのtimer object
    private _timer: number = null;


    /**
     * PUBLIC MEMBER
     */

    // APIを読み込むタグが設置完了しているかどうか。
    public _isReady: boolean = false;

    // YoutubePlayerで作られたYT.player instanceを受け取って格納。
    // TODO : ここ、循環参照
    public _playerStock: IPlayerStack[] = [];

    // 全部共通のmute設定
    public _isMute: boolean = false;


    /**
     * Creates an instance of YoutubePlayerInitialyzer.
     *
     * @param {Function} caller
     *
     * @memberOf YoutubePlayerInitialyzer
     */
    constructor(caller: Function) {
        log('YoutubePlayerChief:::constructor');

        if (YoutubePlayerChief._instance)
            throw new Error('Already exists');
        else if (caller !== YoutubePlayerChief.getInstance)
            throw new Error('must use the getInstance.');


        this.apiCallBack();

        this.appendAPI();
    }


    /**
     * youtube apiのcallbackを設定
     *
     * @private
     *
     * @memberOf YoutubePlayerInitialyzer
     */
    private apiCallBack(): void {
        log('YoutubePlayerChief:::apiCallBack');
        window['onYouTubeIframeAPIReady'] = () => {
            this._isReady = true;
            this.callBackTrigger();
        }
    }



    /**
     * youtube apiをappendする
     *
     * @private
     *
     * @memberOf YoutubePlayerInitialyzer
     */
    private appendAPI(): void {
        log('YoutubePlayerChief:::appendAPI');
        let __tag = document.createElement('script');
        __tag.src = '//www.youtube.com/iframe_api';
        let __firstScriptTag = document.getElementsByTagName('script')[0];
        __firstScriptTag.parentNode.insertBefore(__tag, __firstScriptTag);
    }


    /**
     * callbackをcallする。
     * callbackstackに残っている場合、再帰的に実行する。
     *
     * @private
     *
     * @memberOf YoutubePlayerInitialyzer
     */
    private callBackTrigger(): void {
        clearTimeout(this._timer);

        for (var key in this._callbackStack) {
            if (this._callbackStack.hasOwnProperty(key)) {
                let __cb = this._callbackStack.shift();
                __cb();
            }
        }

        // 再帰処理の実装
        if (this._callbackStack.length > 0) {
            this._timer = setTimeout(() => {
                this.callBackTrigger();
            }, 100);
        };
    }

    /**
     * player stockに追加する。
     *
     * @param {YT.Player} player
     * @param {any} videoId
     *
     * @memberOf YoutubePlayerChief
     */
    public addPlayer(player: YT.Player, videoId): void {
        let _earlyExist = false;
        for (let i = 0; i < this._playerStock.length; i++) {
            var __player = this._playerStock[i];

            if (__player.id === videoId) {
                _earlyExist = true;
            }
        }

        if (!_earlyExist) {
            this._playerStock.push({
                'id': videoId,
                'player': player
            });
        }
    }

    /**
     * player stockから削除する。
     *
     * @param {string} videoId
     *
     * @memberOf YoutubePlayerChief
     */
    public removePlayer(videoId: string): void {
        log('removePlayer', videoId);
        let __index = this._playerStock.map(function (x) { return x.id; }).indexOf(videoId);
        this._playerStock.splice(__index, 1);
        log('removePlayer after', this._playerStock);
    }


    /**
     * 渡されたID以外のYT.Playerを削除する
     *
     * @param {string} videoId
     *
     * @memberOf YoutubePlayerChief
     */
    public stopOther(videoId: string): void {
        log(this._playerStock);
        for (let i = 0; i < this._playerStock.length; i++) {
            var __player = this._playerStock[i];
            if (!__player) {
                log('player stock', i, 'not found. remove');
                log(this._playerStock);
                this._playerStock.slice(i);
                log(this._playerStock);
                continue;
            }

            if (__player.id !== videoId && __player.player.pauseVideo) {
                __player.player.pauseVideo();
            }
        }
    }


    /**
     * 渡されたID以外のYT.Playerを削除する
     *
     * @param {string} videoId
     *
     * @memberOf YoutubePlayerChief
     */
    public destroyOtherPlayer(videoId: string): void {
        for (let i = 0; i < this._playerStock.length; i++) {
            var __player = this._playerStock[i];
            if (__player.id !== videoId) {
                __player.player.destroy();
            }
        }
    }



    /**
     * callbackを追加していく。
     * isReadyならcallbackをtrigger.
     *
     * @param {Function} callback
     * @returns {void}
     *
     * @memberOf YoutubePlayerInitialyzer
     */
    public onReadyCallBack(callback: Function): void {
        this._callbackStack.push(callback);

        if (!this._isReady || !this._callbackStack.length) {
            return;
        }

        this.callBackTrigger();
    }



    /**
     * singleton
     *
     * @static
     * @returns {YoutubePlayerInitialyzer}
     *
     * @memberOf YoutubePlayerInitialyzer
     */
    public static getInstance(): YoutubePlayerChief {
        if (!YoutubePlayerChief._instance)
            YoutubePlayerChief._instance = new YoutubePlayerChief(YoutubePlayerChief.getInstance);

        return YoutubePlayerChief._instance;
    }
}
