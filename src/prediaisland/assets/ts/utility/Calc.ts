/**
 * 端末のpixelRatioを返す
 */
export var scale: number = window.devicePixelRatio;

/**
 * Math.PI / 180を返す
 */
export var TO_RADIAN = Math.PI / 180;

/**
 * 180 / Math.PIを返す
 */
export var TO_DEGREE = 180 / Math.PI;


/**
 * フレーム数を返す
 * currentFarme()を生成してからのフレーム数を返す。
 * currentFarme().getで取得
 * @return{ICurrentFarme} フレーム数を返す。
 */
export function currentFarme(): ICurrentFarme {
    let frame: number = 0;
    return {
        get: function (): number {
            frame++;
            return frame;
        }
    }
}

/**
 * true/falseをランダムに返す。
 * それだけ。本当それだけ
 *
 * @export
 * @returns {boolean}
 */
export function randomBool(): boolean {
    return !!Math.round(Math.random());
}

/**
 * 指定範囲の乱数を返す。
 * @param  {number} min 最小値
 * @param  {number} max 最大値
 * @param  {boolean=false} floor intかfloatか
 * @returns number
 */
export function randomRage(min: number, max: number, floor: boolean = false): number {
    let random = Math.random() * (max - min) + min;
    return floor ? ~~random : random;
}

/**
 * 2点間の距離
 * @param  {IPoint}  Center Centerのx,y座標
 * @param  {IPoint}  pointB PointBのx,y座標
 * @return {number}        距離を返す
 */
export function getDistance(Center: IPoint, pointB: IPoint): number {
    return Math.sqrt(
        Math.pow(pointB.x - Center.x, 2) +
        Math.pow(pointB.y - Center.y, 2)
    );
};

/**
 * 2点間の角度をDegreeで返す。ただし、0~180の中
 * @param  {IPoint}  Center Centerのx,y座標
 * @param  {IPoint}  pointB PointBのx,y座標
 * @return {number}        角度を返す
 */
export function getDegree(Center: IPoint, pointB: IPoint): number {
    let _atan2 = Math.atan2(
        pointB.y - Center.y,
        pointB.x - Center.x
    );
    if (_atan2 < 0) {
        _atan2 += Math.PI * 2;
    }

    return _atan2 * 180 / Math.PI;
}

/**
 * 2点間の角度をRadianで返す。ただし、0~3.14の中
 * @param  {IPoint}  Center Centerのx,y座標
 * @param  {IPoint}  pointB PointBのx,y座標
 * @return {number}        角度を返す
 */
export function getRadian(Center: IPoint, pointB: IPoint): number {
    let _atan2 = Math.atan2(
        pointB.y - Center.y,
        pointB.x - Center.x
    );
    if (_atan2 < 0) {
        _atan2 += Math.PI * 2;
    }

    return _atan2;
}
//
// export function getAtan2
// function MathGetAtan2(y,x){
// 	if(x > 0)	return Math.atan(y/x);
// 	else	return Math.atan(y/x) + Math.PI;
// }
//
// var x = Math.random() * 2 - 1;
// var y = Math.random() * 2 - 1;
// var rot = MathGetAtan2 (y,x) * 180 / Math.PI ;
//
// if(rot > 180)	rot-= 360;
// if(rot <-180)	rot+= 360;
//
// trace(rot);
//
//
/**
 * 角度(radian)と距離が分かっている場合に座標Bを求める
 * radiusは半径である、距離とは半径を求めているのと同じ
 * @param  {number} radian radianで角度
 * @param  {number} radius 中心からの背景
 * @return {IPoint}         xy座標を返す
 */
export function getPoint(radian: number, radius: number): IPoint {
    // let radian = degree * Math.PI / 180;
    let x = Math.cos(radian) * radius;
    let y = Math.sin(radian) * radius;
    return { x, y };
}
