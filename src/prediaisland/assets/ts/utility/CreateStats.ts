/// <reference path="./Interface.ts"/>
import Stats = require('stats.js');

class CreateStats {

    /**
     * CONST MEMBER
     */

    /**
     * STATIC MEMBER
     */

    // singleton instanceを格納します。
    private static _instance: CreateStats = null;

    /**
     * PRIVATE MEMBER
     */

    /**
     * PUBLIC MEMBER
     */

    private stats: any;


    /**
     * Creates an instance of CreateStats.
     *
     * @param {Function} caller
     */
    constructor(caller:Function) {
        if (CreateStats._instance)
            throw new Error('Already exists');
        else if (caller !== CreateStats.getInstance)
            throw new Error('must use the getInstance.');
    }


    /**
     * setup
     *
     * @param {number} mode 0:fps, 1:ms, 2:mb, 3+:custom
     */
    public setup(mode: number): void{
        this.stats = new Stats();
        this.stats.setMode(mode);
        this.stats.domElement.style.position = 'absolute';
        this.stats.domElement.style.left = '0px';
        this.stats.domElement.style.top = '0px';
        document.body.appendChild(this.stats.domElement);
    }


    /**
     * stats begin
     */
    public begin(): void {
        this.stats.begin();
    }


    /**
     * Stats end
     */
    public end(): void {
        this.stats.end();
    }


    /**
     * singlton method
     *
     * @static
     * @returns {CreateStats}
     */
    public static getInstance(): CreateStats {
        if (!CreateStats._instance)
            CreateStats._instance = new CreateStats(CreateStats.getInstance);

        return CreateStats._instance;
    }
}

export = CreateStats;
