export let EVENT_IS_MOBILE = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ? true : false;
// Reference : http://shared-blog.kddi-web.com/activity/221

// mouse event
export let EVENT_CLICK: 'click' = 'click'; // ボタンをクリックしたときに発火
export let EVENT_MOUSE_MOVE: 'mousemove' = 'mousemove'; // カーソルがターゲット内に移動したときに発火
export let EVENT_MOUSE_DOWN: 'mousedown' = 'mousedown'; // ボタンを押下したときに発火
export let EVENT_MOUSE_UP: 'mouseup' = 'mouseup'; // ボタンを離したときに発火
export let EVENT_MOUSE_ENTER: 'mouseenter' | 'touchstart' = !this.IS_MOBILE ? 'mouseenter' : 'touchstart'; // カーソルがターゲット内に侵入してきたときに発火
export let EVENT_MOUSE_LEAVE: 'mouseleave' | 'touchend' = !this.IS_MOBILE ? 'mouseleave' : 'touchend'; // カーソルがターゲット外に出たときに発火
export let EVENT_MOUSE_OVER: 'mouseover' | 'touchstart' = !this.IS_MOBILE ? 'mouseover' : 'touchstart'; // カーソルがターゲット内に侵入してきたときに発火
export let EVENT_MOUSE_OUT: 'mouseout' | 'touchend' = !this.IS_MOBILE ? 'mouseout' : 'touchend'; // カーソルがターゲット外に出たときに発火

// keyboard event
export let EVENT_KEY_PRESS: 'keypress' = 'keypress'; // キーを押して離したときに発火
export let EVENT_KEY_DOWN: 'keydown' = 'keydown'; // キー押下時に発火
export let EVENT_KEY_UP: 'keyup' = 'keyup'; // キーを離したときに発火


// movie event
export let EVENT_movie_ENDED: 'ended' = 'ended'; // おわり。

// dom event
export let EVENT_DOM_FOCUS_IN: 'DOMFocusIn' = 'DOMFocusIn'; // ターゲットがフォーカスを受け取ったとき発火
export let EVENT_DOM_FOCUS_OUT: 'DOMFocusOut' = 'DOMFocusOut'; // ターゲットがフォーカスを失ったとき発火
export let EVENT_DOM_ACTIVATE: 'DOMActivate' = 'DOMActivate'; // ターゲットがアクティブになったとき発火
export let EVENT_DOM_CONTENT_LOADED: 'DOMContentLoaded' = 'DOMContentLoaded';

// input event
export let EVENT_SELECT: 'select' = 'select'; // テキストフィールドで文字が選択されたときに発火
export let EVENT_CHANGE: 'change' = 'change'; // コントロールの値が変化した後に発火
export let EVENT_SUBMIT: 'submit' = 'submit'; // submitボタンが押されたときに発火
export let EVENT_RESET: 'reset' = 'reset'; // resetボタンが押されたときに発火
export let EVENT_FOCUS: 'focus' = 'focus'; // コントロールがフォーカスを受け取ったときに発火
export let EVENT_BLUR: 'blur' = 'blur'; // コントロールがフォーカスを失ったときに発火

// window & body event
export let EVENT_BEFORE_UNLOAD: 'beforeunload' = 'beforeunload'; // ブラウザのアンロード前に実行される
export let EVENT_LOAD: 'load' = 'load'; // 読み込みが完了したときに発火
export let EVENT_SCROLL: 'scroll' = 'scroll'; // コントロール付属のスクロールバー位置が変更されたときに発火
export let EVENT_RESIZE: 'resize' = 'resize'; // コントロールのサイズが変更されたときに発火
export let EVENT_ERROR: 'error' = 'error'; // エラー
export let EVENT_HASH_CHANGE: 'hashchange' = 'hashchange'; // ハッシュチェンジのとき
export let EVENT_OFFLINE: 'offline' = 'offline';
export let EVENT_ONLINE: 'online' = 'online';
export let EVENT_PAGE_SHOW: 'pageshow' = 'pageshow';
export let EVENT_PAGE_HIDE: 'pagehide' = 'pagehide';
export let EVENT_POP_STATE: 'popstate' = 'popstate';
export let EVENT_PUSH_STATE: 'pushstate' = 'pushstate';
export let EVENT_STORAGE: 'storage' = 'storage';


export let EVENT_TRANSITIONEND = (() => {
    let el = document.createElement('fakeelement');
    let transitions = {
        'transition': 'transitionend',
        'OTransition': 'oTransitionEnd',
        'MozTransition': 'transitionend',
        'WebkitTransition': 'webkitTransitionEnd'
    }

    for (let t in transitions) {
        if (el.style[t] !== undefined) {
            return transitions[t];
        }
    }
})()
