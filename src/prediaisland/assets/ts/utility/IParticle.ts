declare var ParticleConfig: IParticle;
interface IParticle {

    debug: boolean;

    particleEmitter: {
        max: number;
        thorottle: number;
    };

    particle: {
        blinks: number[];
        lifeMin: number;
        LifeMax: number;
        scaleMin: number;
        scaleMax: number;
        vectorMin: number;
        vectorMax: number;
        frictionMin: number;
        frinctionMax: number;
        rotationMin: number;
        rotationMax: number;
    };
}
