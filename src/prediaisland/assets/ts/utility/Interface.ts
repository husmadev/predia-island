interface IPoint {
    x: number;
    y: number;
}

interface ISize {
    width: number;
    height: number;
}

interface ICallbackType {
    (argv: number): void;
};


interface IWSJsonData {
    finderid: string;
    x: number;
    y: number;
}

interface IDropInformation {
    finderid: string;
    status: string;
    id: number;
    x: number;
    y: number;
}

interface ICurrentFarme {
    get(): number;
}
interface IteratorResult<T> {
  value?:T;
  done:boolean
}

interface ICallbackType { (argv: number): void; };
interface ICallbackEvent { (event: Event): void; };

interface Riot {
    route: {
        (toOrCallback: string | Function): void
        (callback: Function): void
        parser: {
            (args): any;
        }
    };

}

interface JQueryEasingFunction {
    (...args: any[]): number;
}

interface JQueryEasingFunctions {
    def: any;
}

interface JQuery {
    isotope(val: any): JQuery;
}

// tslint:enable
//
// declare module 'FastClick' {
//     export = FastClick;
// }

// declare module 'gsap' {
//     // interface TweenLite{}
//     // interface TweenMax{ }
//     // interface TimelineMax{ }
// }
// declare module 'ScrollToPlugin' {}
declare function require(x: string): any;


