
class MyEvent {
    private _type: string;
    private _target: any;

    constructor(type: string, targetObj: any) {
        this._type = type;
        this._target = targetObj;
    }

    public getTarget(): any {
        return this._target;
    }

    public getType(): string {
        return this._type;
    }
}

export = MyEvent;