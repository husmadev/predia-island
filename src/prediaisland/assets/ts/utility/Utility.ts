import * as $ from 'jquery';
import { Promise } from 'es6-promise';

/**
 * css transition,animatinのcallbackのブラウザごとの差異を吸収
 * @type {Object}
 */
interface IVendorPrefixEventType {
    transitionend: string[];
    animationend: string[];
}

export let matchMaxWidth = window.matchMedia('(max-width:859px)').matches;

export var vendorPrefixEventType: IVendorPrefixEventType = {
    'transitionend': ['webkitTransitionEnd', 'transitionend'],
    'animationend': ['webkitAnimationEnd', 'animationend']
};


export var vendorPrefixs: string[] = ['webkit', 'moz', 'ms', 'o'];

/**
 * console.logの隠蔽
 * @param  {[type]} console [description]
 * @return {[type]}         [description]
 */
export var log = window['debugMode'] ? console.log.bind(console) : () => { /* some */ };
export var info = window['debugMode'] ? console.info.bind(console) : () => { /* some */ };
export var error = window['debugMode'] ? console.error.bind(console) : () => { /* some */ };

export var ua = navigator.userAgent.toLowerCase();
export var ver = navigator.appVersion.toLowerCase();

export var isiPhone = (ua.indexOf('iphone') > -1);
export var isiPad = (ua.indexOf('ipad') > -1);
export var isIOS = isiPhone || isiPad;
export var isAndroid = (ua.indexOf('android') > -1) && (ua.indexOf('mobile') > -1);
export var isAndroidTablet = (ua.indexOf('android') > -1) && (ua.indexOf('mobile') === -1);
export var isSmartPhone = isiPhone || isAndroid;
export var isTablet = isiPad || isAndroidTablet;
export var isTouchDevice = isSmartPhone || isTablet;

export var isMSIE = (ua.indexOf('msie') > -1) && (ua.indexOf('opera') === -1);
export var isIE8 = isMSIE && (ver.indexOf('msie 8.') > -1);
export var isIE9 = isMSIE && (ver.indexOf('msie 9.') > -1);
export var isIE10 = isMSIE && (ver.indexOf('msie 10.') > -1);
export var isIE11 = (ua.indexOf('trident/7') > -1);
export var isIE = isMSIE || isIE11;
export var isEdge = (ua.indexOf('edge') > -1);
export var isChrome = (ua.indexOf('chrome') > -1) && (ua.indexOf('edge') === -1);
export var isFirefox = (ua.indexOf('firefox') > -1);
export var isSafari = (ua.indexOf('safari') > -1) && (ua.indexOf('chrome') === -1);
export var isOpera = (ua.indexOf('opera') > -1);

export var isMac = navigator.platform.indexOf('Mac') > -1;
export var isWindows = ua.indexOf('windows') > -1;


// Add UA class
var __detectClass = '';
if (isMac) { __detectClass += ' mac'; }
if (isWindows) {
    __detectClass += ' win';
    if (isMSIE) { __detectClass += ' ie'; }
    if (isMSIE && ua.indexOf('trident') > -1) { __detectClass += ' gte_ie8'; }
    if (isMSIE && ua.indexOf('trident') === -1) { __detectClass += ' lte_ie8'; }
    if (isIE8) { __detectClass += ' ie8'; }
    if (isIE9) { __detectClass += ' ie9'; }
    if (isIE10) { __detectClass += ' ie10'; }
    if (isIE11) { __detectClass += ' ie11'; }
}
if (isChrome) { __detectClass += ' chrome'; }
if (isSafari) { __detectClass += ' safari'; }
if (isFirefox) { __detectClass += ' firefox'; }
if (isiPad) { __detectClass += ' ipad'; }
if (isiPhone) { __detectClass += ' iphone'; }
if (isAndroid) { __detectClass += ' android'; }
if (isSmartPhone) { __detectClass += ' smartphone'; }
if (isTablet) { __detectClass += ' tablet'; }

document.documentElement.className += __detectClass;



/**
 * 破壊的な配列のシャップル
 * @param  {Array<any>} array
 * @returns Array
 */
export function shuffle(array: Array<any>): Array<any> {
    var m: number = array.length,
        t: Array<any>,
        i: number;
    while (m) {
        i = Math.floor(Math.random() * m--);
        // And swap it with the current element.
        t = array[m];
        array[m] = array[i];
        array[i] = t;
    }
    return array;
}


export function empty(mixedVar) {
    //  discuss at: http://locutus.io/php/empty/
    // original by: Philippe Baumann
    //    input by: Onno Marsman (https://twitter.com/onnomarsman)
    //    input by: LH
    //    input by: Stoyan Kyosev (http://www.svest.org/)
    // bugfixed by: Kevin van Zonneveld (http://kvz.io)
    // improved by: Onno Marsman (https://twitter.com/onnomarsman)
    // improved by: Francesco
    // improved by: Marc Jansen
    // improved by: Rafał Kukawski (http://blog.kukawski.pl)
    //   example 1: empty(null)
    //   returns 1: true
    //   example 2: empty(undefined)
    //   returns 2: true
    //   example 3: empty([])
    //   returns 3: true
    //   example 4: empty({})
    //   returns 4: true
    //   example 5: empty({'aFunc' : function () { alert('humpty'); } })
    //   returns 5: false
    var undef;
    var key;
    var i;
    var len;
    var emptyValues = [undef, null, false, 0, '', '0']
    for (i = 0, len = emptyValues.length; i < len; i++) {
        if (mixedVar === emptyValues[i]) {
            return true;
        }
    }
    if (typeof mixedVar === 'object') {
        for (key in mixedVar) {
            if (mixedVar.hasOwnProperty(key)) {
                return false;
            }
        }
        return true;
    }
    return false;
}




/**
 * ID名からHTMLElementを返す
 * @param  {string}      findName ID名。#は抜く
 * @return {HTMLElement}          [description]
 */
export function getByID(findName: string): HTMLElement {

    var _dom: HTMLElement = document.getElementById(findName);

    return _dom;
}

/**
 * query名からNodeListを返す
 * @param  {string}      findName query
 * @return {NodeList}          [description]
 */
export function getByQuery(findName: string): NodeList {
    var _dom: NodeList = document.querySelectorAll(findName);
    return _dom;
}

/**
 * query名からNodeListを返す
 * @param  {string}      findName query
 * @return {Element}          [description]
 */
export function selectEle(findName: string): Element {
    var _dom: Element = document.querySelector(findName);
    return _dom;
}
/**
 * query名からNodeListを返す
 * @param  {string}      findName query
 * @return {NodeList}          [description]
 */
export function selectEleAll(findName: string): NodeList {
    var _dom: NodeList = document.querySelectorAll(findName);
    return _dom;
}

/**
 * elementにクラスを付与する
 * @param {HTMLElement} element   [description]
 * @param {string}      className [description]
 */
export function addClass(element: HTMLElement, className: string): void {
    element.classList.add(className);
}


/**
 * elementからクラスを削除する
 * @param {HTMLElement} element   [description]
 * @param {string}      className [description]
 */
export function removeClass(element: HTMLElement, className: string): void {
    element.classList.remove(className);
}

/**
 * 正規表現で指定したclassを置き換える。
 *
 * @export
 * @param {HTMLElement} element DOMを指定する
 * @param {string} classGroup
 * @param {string} className
 */
export function toggleClassRegex(element: HTMLElement, classGroup: string, className: string): void {
    const _regex: RegExp = new RegExp(classGroup, 'i');
    var _hasClassName: string = '';

    // http://stackoverflow.com/questions/21390675/check-if-an-element-contains-a-class-by-regex-with-classlist
    if (element.className.split(' ').some(function (c) {
        _hasClassName = c;
        return _regex.test(c);
    })) {
        element.classList.remove(_hasClassName);
    }
    element.classList.add(classGroup + className);
}


/**
 * cssのベンダープレフィクス付ける
 * @param  {HTMLElement} _el
 * @param  {string} _prop
 * @param  {string} _val
 */
export function setVendor(_el: HTMLElement, _prop: string, _val: string) {
    for (var i = 0; i < vendorPrefixs.length; i++) {
        _el.style[vendorPrefixs[i] + _prop[0].toUpperCase() + _prop.substr(1)] = _val;
    }
    _el.style[_prop] = _val;
}


/**
 * bodyからのelementの座標を取得する。position:absoluteなどになっている場合に使える
 * @param  {Element} _el
 * @returns IPoint
 */
export function elementPositionOnBody(_el: Element): IPoint {
    let bodyRect = document.body.getBoundingClientRect(),
        elemRect = _el.getBoundingClientRect();
    return {
        x: elemRect.left - bodyRect.left,
        y: elemRect.top - bodyRect.top
    };
}


/**
 * DOMがviewport内にいるかどうか
 * @param  {Element} _el
 * @param  {boolean=false} _inPart 一部でも入っていればOKな場合
 * @returns boolean
 */
export function isElementInViewport(_el: Element, _windowSize: ISize, _percent: number = 1, _inPart: boolean = false): boolean {

    let rect = _el.getBoundingClientRect();
    let _rectLeft = rect.left;
    if (rect.left === rect.right) {
        _rectLeft = 0;
    }

    let _hLeft = _windowSize.width - _windowSize.width * _percent;
    let _hRight = _windowSize.width * _percent;

    let _vTop = _windowSize.height - _windowSize.height * _percent;
    let _vBottom = _windowSize.height * _percent;

    if (_inPart) {
        // 縦横一部でも入っている
        return (rect.top >= _vTop && rect.top <= _vBottom || // 上がエリア内
            rect.bottom >= _vTop && rect.bottom <= _vBottom || // 下がエリア内
            rect.top <= _vTop && rect.bottom >= _vBottom) // 上下ともエリアの外に溢れてる
            &&
            (_rectLeft >= _hLeft && _rectLeft <= _hRight ||
                rect.right >= _hLeft && rect.right <= _hRight ||
                _rectLeft <= _hLeft && rect.right >= _hRight);
    } else {
        // 縦横全部が入っている
        return rect.top >= _vTop && rect.bottom <= _vBottom && _rectLeft >= _hLeft && rect.right <= _hRight;
    }
}


/**
 * localStorageにobjectを格納します。
 *
 * @export
 * @param {string} key
 * @param {Object} data
 */
export function setLocalStorage(key: string, data: Object): void {
    localStorage.setItem(key, JSON.stringify(data));
}


/**
 * localStorageからobjectを取得します。
 *
 * @export
 * @param {string} key
 * @returns {Object}
 */
export function getLocalStorage(key: string): Object {
    let _getData: any = localStorage.getItem(key);
    let _parseData: Object = JSON.parse(_getData);
    return _parseData;
}


/**
 * localStorageが使用可能かチェックします。
 *
 * @export
 * @returns {boolean}
 */
export function isLocalStorageSuppoeted(): boolean {
    if (!window.sessionStorage) {
        return false;
    }

    let testKey = 'test';
    try {
        window.sessionStorage.setItem(testKey, '1');
        window.sessionStorage.removeItem(testKey);
        return true;
    } catch (error) {
        return false;
    }
}

/**
 * HTMLタグをエスペープします。
 *
 * @export
 * @param {string} str
 * @returns {string}
 */
export function escapeHtml(str: string): string {
    str = str.replace(/&/g, '&amp;');
    str = str.replace(/</g, '&lt;');
    str = str.replace(/>/g, '&gt;');
    str = str.replace(/"/g, '&quot;');
    str = str.replace(/'/g, '&#39;');
    return str;
}


/**
 * imgタグで読み込んだsvgをinline svgに置換します。
 *
 * @export
 * @param {NodeList} _$target NodeListタイプを渡す
 * @param {Function} _complete すべての置換が終わった時のcallbackを渡す
 */
export function replaceSvg2img(_$target: NodeList, _complete: Function) {

    let _nodes: NodeList = _$target;
    let _length = _nodes.length;
    let _loadCount = 0;
    let _nodeParams: {
        _img: HTMLImageElement,
        _id: string,
        _class: string,
        _src: string
    }[] = [];

    let _successFunc = function (_responce: any, identifier: any) {
        let _div = document.createElement('div');
        let _svg: SVGElement;
        _div.innerHTML = _responce;
        _svg = <SVGElement>_div.firstChild;

        if (!_svg) {
            return false;
        }

        let _viewbox: string[] = _svg.getAttribute('viewBox').split(' ');

        _viewbox[2] = '' + Math.ceil(+_viewbox[2]);
        _viewbox[3] = '' + (Math.ceil(+_viewbox[3]) + 1); // 1px足りないことがおおい

        _svg.setAttribute('viewBox', _viewbox.join(' '));

        _svg.setAttribute('width', _viewbox[2]);
        _svg.setAttribute('height', _viewbox[3]);

        if (typeof _nodeParams[identifier]._id !== 'undefined') {
            _svg.setAttribute('id', _nodeParams[identifier]._id);
        }

        if (typeof _nodeParams[identifier]._class !== 'undefined') {
            _svg.setAttribute('class', _nodeParams[identifier]._class + ' replaced-svg');
        }

        // Remove any invalid XML tags as per http://validator.w3.org
        _svg.removeAttribute('xmlns:a');
        // Replace image with new SVG
        $(_nodeParams[identifier]._img).replaceWith(_div.innerHTML)
        // _nodeParams[identifier]._img.outerHTML = _div.innerHTML;
        _loadCount++;
        if (_loadCount === _length) {
            _complete();
        }
    };

    let _errorFunc = function (_responce: any, identifier: any) {
        _nodeParams[identifier]._img.className += ' fail-loadsvg';
        this.error(_responce);
    };


    for (let i = 0; i < _length; i++) {
        let _node: HTMLImageElement = <HTMLImageElement>_nodes[i];
        _nodeParams[i] = {
            _img: _node,
            _id: _node.id,
            _class: _node.className,
            _src: _node.src
        }

        loadXHR('get', _node.src, _successFunc, _errorFunc, i);
    }
};


/**
 *
 * XMLHttpRequestでfileを読み込みます。
 * identifierをつけるとcallbackで識別できるよう、responseと一緒に返します。
 * @export
 * @param {string} [_type='get']
 * @param {string} _file
 * @param {Function} _success
 * @param {Function} _fail
 * @param {(string|number)} [identifier] responseといっしょに返します。
 */
export function loadXHR(_type: string = 'get', _file: string, _success: Function, _fail: Function, identifier?: string | number): void {

    const xhr: XMLHttpRequest = new XMLHttpRequest();
    xhr.onreadystatechange = () => {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            if (xhr.status === 200) {
                _success(xhr.responseText, identifier);
            } else if (xhr.status === 400) {
                _fail(xhr.responseText, identifier);
            } else {
                _fail(xhr.responseText, identifier);
            }
        }
    };

    xhr.open(_type, _file, true);
    xhr.send();
}

// /**
//  * css animation/transitionのeventlistner追加
//  * @param {HTMLElement}  dom      [description]
//  * @param {string}       type     [description]
//  * @param {callbackType} callback [description]
//  * TODO: 子要素のeventを拾うかどうか
//  * TODO: 複数のコールバックがある場合何回まで拾うか。0で全部。
//  */
// export function addEvent( dom: HTMLElement, type: string, callback: ICallbackType ): void {
//
//     var listener = CSS_CALLBACK[ type ];
//
//     for (var i = 0; i < listener.length; i++) {
//         dom.addEventListener( listener[ i ], () => { callback( i ); });
//     }
// }

// /**
// * css animation/transitionのeventlistnerを削除
//  * @param {HTMLElement}  dom      [description]
//  * @param {string}       type     [description]
//  * @param {callbackType} callback [description]
//  */
// export function removeEvent( dom: HTMLElement, type: string, callback: ICallbackType ): void {
//
//     var listener = this.CSS_CALLBACK[ type ];
//
//     for (var i = 0; i < listener.length; i++) {
//         dom.removeEventListener( listener[ i ], () => { callback( i ); });
//     }
// }


/**
 * obj1に存在しないプロパティをobj2からobj1へマージする
 * @param  {Object} obj1 ベースのobject。破壊的に結合
 * @param  {Object} obj2 これをobj1にがっちゃんこ
 */
export function mergeObject(obj1: Object, obj2: Object): Object {
    let _newObj: Object = obj1;
    if (!obj2) {
        obj2 = {};
    }
    for (var attrname in obj2) {
        if (obj2.hasOwnProperty(attrname) && !obj1.hasOwnProperty(attrname)) {
            _newObj[attrname] = obj2[attrname];
        }
    }

    return _newObj;
};

export function zeroPadding(_number: number, length: number): string {
    return (Array(length).join('0') + _number).slice(-length);
}

export function clone(obj: Object): Object {
    var copy;

    // Handle the 3 simple types, and null or undefined
    if (null == obj || 'object' !== typeof obj) {
        return obj;
    }

    // Handle Date
    if (obj instanceof Date) {
        copy = new Date();
        copy.setTime(obj.getTime());
        return copy;
    }

    // Handle Array
    if (obj instanceof Array) {
        copy = [];
        for (var i = 0, len = obj.length; i < len; i++) {
            copy[i] = clone(obj[i]);
        }
        return copy;
    }

    // Handle Object
    if (obj instanceof Object) {
        copy = {};
        for (var attr in obj) {
            if (obj.hasOwnProperty(attr)) {
                copy[attr] = clone(obj[attr]);
            }
        }
        return copy;
    }

    throw new Error('Unable to copy obj! Its type isn\'t supported.');
}

// main function
export function scrollToY(scrollTargetY, speed, easing) {
    // scrollTargetY: the target scrollY property of the window
    // speed: time in pixels per second
    // easing: easing equation to use

    var scrollY = window.scrollY,
        scrollTargetY = scrollTargetY || 0,
        speed = speed || 2000,
        easing = easing || 'easeOutSine',
        currentTime = 0;

    // min time .1, max time .8 seconds
    var time = Math.max(.1, Math.min(Math.abs(scrollY - scrollTargetY) / speed, .8));

    // easing equations from https://github.com/danro/easing-js/blob/master/easing.js
    var PI_D2 = Math.PI / 2,
        easingEquations = {
            easeOutSine: function (pos) {
                return Math.sin(pos * (Math.PI / 2));
            },
            easeInOutSine: function (pos) {
                return (-0.5 * (Math.cos(Math.PI * pos) - 1));
            },
            easeInOutQuint: function (pos) {
                if ((pos /= 0.5) < 1) {
                    return 0.5 * Math.pow(pos, 5);
                }
                return 0.5 * (Math.pow((pos - 2), 5) + 2);
            }
        };

    // add animation loop
    function tick() {
        currentTime += 1 / 60;

        var p = currentTime / time;
        var t = easingEquations[easing](p);

        if (p < 1) {
            window.requestAnimationFrame(tick.bind(this));

            window.scrollTo(0, scrollY + ((scrollTargetY - scrollY) * t));
        } else {
            window.scrollTo(0, scrollTargetY);
        }
    }

    // call it once to get started
    tick();
}


/**
 * disableScrollの関数
 */
var keys = {
    37: 1,
    38: 1,
    39: 1,
    40: 1
};

function preventDefault(e) {
    e = e || window.event;
    if (e.preventDefault) {
        e.preventDefault();
    }
    e.returnValue = false;
}

function preventDefaultForScrollKeys(e) {
    if (keys[e.keyCode]) {
        preventDefault(e);
        return false;
    }
}


/**
 * @param  {Boolean} isDisable スクロールをOFFにする
 * @returns void
 */
export function disableScroll(isDisable: Boolean): void {
    if (isDisable) {
        window.onwheel = preventDefault; // modern standard
        window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
        window.ontouchmove = preventDefault; // mobile
        document.onkeydown = preventDefaultForScrollKeys;
    } else {
        window.onmousewheel = document.onmousewheel = null;
        window.onwheel = null;
        window.ontouchmove = null;
        document.onkeydown = null;
    }
}



/**
 * transitionend でresolveを返す。
 * @param  {HTMLElement} dom
 * @param  {string} eventType
 */
export function onPromiseHandler(dom: HTMLElement, eventType: string): Promise<any> {
    let __event: string[] = this.CSS_CALLBACK[eventType] || [eventType];

    return new Promise((resolve, reject) => {

        let __onComplete = (e) => {

            for (var i = 0; i < __event.length; i++) {
                dom.addEventListener(__event[i], __onComplete);
            }

            resolve();
        }


        for (var i = 0; i < __event.length; i++) {
            dom.addEventListener(__event[i], __onComplete);
        }

    });

}


/**
 * css animation/transitionのeventlistner追加
 * @param {HTMLElement}  dom      [description]
 * @param {string}       type     [description]
 * @param {callbackType} callback [description]
 * TODO: 子要素のeventを拾うかどうか
 * TODO: 複数のコールバックがある場合何回まで拾うか。0で全部。
 */
export function addCssEventLister(dom: HTMLElement, type: string, callback: ICallbackType): void {

    var listener = this.CSS_CALLBACK[type];

    for (var i = 0; i < listener.length; i++) {
        dom.addEventListener(listener[i], () => {
            callback(i);
        });
    }
}

/**
 * css animation/transitionのeventlistnerを削除
 * @param {HTMLElement}  dom      [description]
 * @param {string}       type     [description]
 * @param {callbackType} callback [description]
 */
export function removeCssEventLister(dom: HTMLElement, type: string, callback: ICallbackType): void {

    var listener = this.CSS_CALLBACK[type];

    for (var i = 0; i < listener.length; i++) {
        dom.removeEventListener(listener[i], () => {
            callback(i);
        });
    }
}
/**
 * css animation/transitionのeventlistner追加
 * @param {HTMLElement}  dom      [description]
 * @param {string}       type     [description]
 * @param {callbackType} callback [description]
 * TODO: 子要素のeventを拾うかどうか
 * TODO: 複数のコールバックがある場合何回まで拾うか。0で全部。
 */
export function addQueryAllEventLister(dom: NodeList, type: string, callback: ICallbackEvent): void {
    for (var i = 0; i < dom.length; i++) {
        dom[i].addEventListener(type, (e: Event) => {
            callback(e);
        });
    }
}

/**
 * css animation/transitionのeventlistnerを削除
 * @param {HTMLElement}  dom      [description]
 * @param {string}       type     [description]
 * @param {callbackType} callback [description]
 */
export function removeQueryAllEventLister(dom: NodeList, type: string, callback: ICallbackEvent): void {
    for (var i = 0; i < dom.length; i++) {
        dom[i].removeEventListener(type, (e: Event) => {
            callback(e);
        });
    }
}

export function getUrlVars(): String[] {
    let vars: String[] = [],
        max: number = 0,
        hash: string[] = [],
        array: string[] = [];

    let url = window.location.search;

    // ?を取り除くため、1から始める。複数のクエリ文字列に対応するため、&で区切る
    hash = url.slice(1).split('&');
    max = hash.length;
    for (var i = 0; i < max; i++) {
        array = hash[i].split('='); // keyと値に分割。
        vars.push(array[0]); // 末尾にクエリ文字列のkeyを挿入。
        vars[array[0]] = array[1]; // 先ほど確保したkeyに、値を代入。
    }
    return vars;
}

// /**
//  * <script>タグをheadに挿入する
//  * @param  {string}    pathToScript jsまでのpath
//  * @param  {string = 'text/javascript'} type type名。なければjs
//  */
// export function appendScript( pathToScript: string, type: string = 'text/javascript', callBack: Function ): void {
//     let _head: HTMLHeadElement = document.getElementsByTagName('head')[0];
//     let _js: HTMLScriptElement = document.createElement('script');
//     _head.appendChild( _js );
//     _js.onload  = function(e) { this.log('unkojijiji') };
//     _js.type = type;
//     _js.src = pathToScript;
// }
//
//


/**
 * flash playerのインストールを確認して、バージョンを返す
 * @param {HTMLElement}  dom      [description]
 * @param {string}       type     [description]
 * @param {callbackType} callback [description]
 */
export function getFlashPlayerVersion(): number {
    // [All] Flash Player is NOT installed.
    let version: number = 0;
    try {
        // [IE] Flash Player is installed.
        let AXO = new window['ActiveXObject']('ShockwaveFlash.ShockwaveFlash');
        version = AXO.GetVariable('$version').match(/([0-9]+)/)[0];
    } catch (e) {
        if (navigator.mimeTypes['application/x-shockwave-flash'] !== undefined) {
            if (navigator.plugins['Shockwave Flash'] && navigator.mimeTypes.length) {
                // [Other Browser] Flash Player is installed.
                version = navigator.plugins['Shockwave Flash'].description.match(/([0-9]+)/)[0];
            }
        }
    }
    return version;
};




/**
 * 
 */
export function getCookieObject(): any {
    let docCookies:any;
    return docCookies = {
        getItem: function (sKey) {
            if (!sKey || !this.hasItem(sKey)) { return null; }
            return unescape(document.cookie.replace(new RegExp("(?:^|.*;\\s*)" + escape(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*((?:[^;](?!;))*[^;]?).*"), "$1"));
        },
        setItem: function (sKey, sValue, vEnd, sPath, sDomain, bSecure) {
            if (!sKey || /^(?:expires|max\-age|path|domain|secure)$/i.test(sKey)) { return; }
            var sExpires = "";
            if (vEnd) {
            switch (vEnd.constructor) {
                case Number:
                sExpires = vEnd === Infinity ? "; expires=Tue, 19 Jan 2038 03:14:07 GMT" : "; max-age=" + vEnd;
                break;
                case String:
                sExpires = "; expires=" + vEnd;
                break;
                case Date:
                sExpires = "; expires=" + vEnd.toGMTString();
                break;
            }
            }
            document.cookie = escape(sKey) + "=" + escape(sValue) + sExpires + (sDomain ? "; domain=" + sDomain : "") + (sPath ? "; path=" + sPath : "") + (bSecure ? "; secure" : "");
        },
        removeItem: function (sKey, sPath) {
            if (!sKey || !this.hasItem(sKey)) { return; }
            document.cookie = escape(sKey) + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT" + (sPath ? "; path=" + sPath : "");
        },
        hasItem: function (sKey) {
            return (new RegExp("(?:^|;\\s*)" + escape(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=")).test(document.cookie);
        },
        keys: /* optional method: you can safely remove it! */ function () {
            var aKeys = document.cookie.replace(/((?:^|\s*;)[^\=]+)(?=;|$)|^\s*|\s*(?:\=[^;]*)?(?:\1|$)/g, "").split(/\s*(?:\=[^;]*)?;\s*/);
            for (var nIdx = 0; nIdx < aKeys.length; nIdx++) { aKeys[nIdx] = unescape(aKeys[nIdx]); }
            return aKeys;
        }
    };
}