declare module 'pjax-api' {
  export default Pjax;

  export class Pjax {
    static assign(url: string, config: Config): any;
    static replace(url: string, config: Config): any;
    constructor(
      config: Config,
      io?: {
        document: Document;
      })
    assign(url: string): any;
    replace(url: string): any;
  }

  export interface Config {
    areas?: string[];
    link?: string;
    filter?: (el: HTMLAnchorElement) => boolean;
    form?: string;
    replace?: string;
    fetch?: {
      timeout?: number;
      wait?: number;
    };
    rewrite?: (doc: Document, area: string, host: string) => void;
    update?: {
      head?: string;
      css?: boolean;
      script?: boolean;
      ignore?: string;
      reload?: string;
      logger?: string;
    };
    fallback?: (target: HTMLAnchorElement | HTMLFormElement | Window, reason: any) => void;
    sequence?: Sequence<any, any, any>;
    balance?: {
      bounds?: string[];
      weight?: number;
      random?: number;
      client?: {
        hosts?: string[];
        support?: {
          balance?: RegExp;
          redirect?: RegExp;
        };
        cookie?: {
          balance?: string;
          redirect?: string;
        };
      };
      server?: {
        header?: string;
        respite?: number;
        expires?: number;
      };
    };
    store?: {
      expiry?: number;
    };
    scope?: { [path: string]: Config | any; };
  }

  export interface Sequence<a, b, c> {
    fetch: (result: void, request: { host: string; path: string; method: string; data: FormData | any; }) => Promise<a>;
    unload: (result: a, response: { headers: { [field: string]: string; }; document: Document; }) => Promise<b>;
    ready: (result: b) => Promise<c>;
    load: (result: c) => void;
  }

}
