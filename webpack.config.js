// import Config, { environment } from 'webpack-config';
var _interopRequire = function (obj) { return obj && obj.__esModule ? obj["default"] : obj; };
var Config = _interopRequire(require("webpack-config"));
var environment = require('webpack-config').environment;

var webpack = require('webpack');
var minimist = require('minimist');
var args = minimist(process.argv.slice(2));
var dir =  ( args['d'].replace(/\/$/,'') || '' );

environment.setAll({
    env: () => args['env'] || 'dev',
    dir: () => dir
});

module.exports = new Config().extend('webpack/webpack.[env].config.js');
