// see migrating https://webpack.js.org/guides/migrating/

// import Config, { environment } from 'webpack-config';
var _interopRequire = function (obj) {
    return obj && obj.__esModule ? obj["default"] : obj;
};
var Config = _interopRequire(require("webpack-config"));
var environment = require('webpack-config').environment;

var path = require("path");
var minimist = require('minimist');
var webpack = require('webpack');


/** *******************************************
 * 本番と開発共通
 ******************************************* */

// commonファイルを生成
var commonsChunkPlugin = new webpack.optimize.CommonsChunkPlugin({
    name: 'commons',
    filename: 'commons.js',
    minChunks: 2
});

// 指定した変数を他のモジュール内で使用できるようにする。globalには置かない。
// jquery : 'jQuery'ってするとjQueryを'jquery'としてrequireできる。
// require('jquery')
var providePlugin = new webpack.ProvidePlugin({
    'Promise': 'es6-promise', // Thanks Aaron (https://gist.github.com/Couto/b29676dd1ab8714a818f#gistcomment-1584602)
    'fetch': 'imports?this=>global!exports?global.fetch!whatwg-fetch'
});

// export default
module.exports = new Config().merge({

    // entry pointとして読み込むファイル
    // 生成される名前:読み込むファイル
    entry: {
        main: './src[dir]/assets/ts/Main',
        top: './src[dir]/assets/ts/PageTop',
        sec: './src[dir]/assets/ts/PageSec'
    },

    // entry[FOOBAR]をfilenameとして吐き出す。
    output: {
        path: path.resolve(__dirname, '../dist[dir]/assets/js/'),
        filename: '[name].js',
        sourceMapFilename: 'map/[name].map'
    },

    // 参照を解決させる。
    resolve: {
        modules: [
            'node_modules',
            path.resolve(__dirname, './src[dir]/')
        ],
        extensions: ['.ts', '.tsx', '.js', '.tag'],
        alias: {
        //     // bower.jsonで解決できない場合ここにmobule名:pathをかく
        //     // http://im0-3.hatenablog.com/entry/2015/04/21/232107
        //     // 参考⬇︎
            ScrollToPlugin: 'gsap/ScrollToPlugin',
        //     createjs : 'PreloadJS/lib/preloadjs-0.6.2.combined',
        }
    },

    // globalを参照させたいobject,packされなくなる。
    externals: {
        "jquery": "jQuery"
    },

    plugins: [
        commonsChunkPlugin,
        providePlugin
    ],

    module: {
        rules: [{
            test: /\.tsx?$/,
            exclude: /node_modules/,
            use: 'light-ts-loader'
        }]
    }
});
