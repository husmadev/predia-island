// import Config, { environment } from 'webpack-config';
var _interopRequire = function (obj) {
    return obj && obj.__esModule ? obj["default"] : obj;
};
var Config = _interopRequire(require("webpack-config"));
var environment = require('webpack-config').environment;

var path = require("path");
var webpack = require('webpack');


/** *******************************************
 * 開発用
 ******************************************* */

// compile時にエラーが出たらskipする。
var noErrors = new webpack.NoEmitOnErrorsPlugin();
var HotModuleReplacement = new webpack.HotModuleReplacementPlugin();
var LoaderOptions = new webpack.LoaderOptionsPlugin({ debug: true });


module.exports = new Config().extend('webpack/webpack.base.config.js').merge({
    devtool: '#source-map',
    output: {
        pathinfo: true
    },
    plugins: [
        noErrors,
        HotModuleReplacement,
        LoaderOptions
    ]
});
