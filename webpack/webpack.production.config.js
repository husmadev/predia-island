// import Config, { environment } from 'webpack-config';
var _interopRequire = function (obj) {
    return obj && obj.__esModule ? obj["default"] : obj;
};
var Config = _interopRequire(require("webpack-config"));
var environment = require('webpack-config').environment;

var path = require("path");
var webpack = require('webpack');


/** *******************************************
 * 本番用
 ******************************************* */

// compile時にuglifyでminimizeする。
var uglifyJsPlugin = new webpack.optimize.UglifyJsPlugin({
    mangle: true,
    output: {
        comments: require('uglify-save-license')
    },
    compress: {
        warnings: false
    }
});

// ファイルを細かく分析し、まとめられるところはできるだけまとめてコードを圧縮する。
var AggressivePlugin = new webpack.optimize.AggressiveMergingPlugin();


module.exports = new Config().extend('webpack/webpack.base.config.js').merge({
    plugins: [
        uglifyJsPlugin,
        AggressivePlugin
    ]
});
